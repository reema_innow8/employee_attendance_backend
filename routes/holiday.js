"use strict";
const express = require("express");
const router = express.Router();
const HC = require("../controllers/holiday"); // holiday controller

router.post("/add", HC.add);
router.post("/remove", HC.remove);
router.post("/edit", HC.edit);
router.post("/fetch", HC.fetch);

module.exports = router;