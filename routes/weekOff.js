"use strict";
const express = require("express");
const router = express.Router();
const WC = require("../controllers/weekOff"); // week-off controller

router.post("/add", WC.add);
router.post("/edit", WC.edit);
router.post("/fetch", WC.fetch);

module.exports = router;