"use strict";
const express = require("express");
const router = express.Router();
const LC = require("../controllers/leave"); // leave controller

router.post("/apply", LC.apply);
router.post("/fetch", LC.fetch);
router.post("/reply", LC.reply);
router.post("/cancellation-request", LC.cancellationRequest);

module.exports = router;