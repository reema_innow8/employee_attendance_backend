"use strict";
const express = require("express");
const router = express.Router();
const AC = require("../controllers/attendance"); // attendance controller

router.post("/fetch", AC.fetch);
router.post("/fetch-all", AC.fetchAll);
router.post("/edit", AC.edit);

module.exports = router;