"use strict";
const express = require("express");
const router = express.Router();
const NC = require("../controllers/notification"); // notification controller

router.post("/fetch", NC.getNotifications);
router.post("/read", NC.read);

module.exports = router;