"use strict";
const express = require("express");
const router = express.Router();
const EC = require("../controllers/employee");   // employee controller
const attendance = require("../index");

router.post("/add", EC.add);
router.post("/org-employees", EC.fetch);
router.post("/remove", EC.remove);
router.post("/edit", EC.edit);
router.post("/upload-image", EC.uploadImage);
router.post("/attendance", attendance.upload.single('attendance'), EC.attendance);

module.exports = router;