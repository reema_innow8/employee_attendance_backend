"use strict";
const express = require("express");
const router = express.Router();
const SC = require("../controllers/salary"); // salary controller

router.post("/add", SC.add);
router.post("/edit", SC.edit);
router.post("/fetch", SC.fetch);
router.post("/compile", SC.compile);
router.post("/download", SC.download);

module.exports = router;