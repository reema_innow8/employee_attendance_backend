"use strict";
const auth = require("./auth");
const employee = require("./employee");
const attendance = require("./attendance");
const holiday = require("./holiday");
const weekOff = require("./weekOff");
const leave = require("./leave");
const salary = require("./salary");
const notification = require("./notification");

//export this router to use in our app.js
module.exports = app => {
  app.use("/auth", auth);
  app.use("/employee", employee);
  app.use("/attendance", attendance);
  app.use('/holiday', holiday);
  app.use('/week-off', weekOff);
  app.use('/leave', leave);
  app.use('/salary', salary);
  app.use('/notification', notification);
};
