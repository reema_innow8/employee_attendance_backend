"use strict";
const express = require("express");
const router = express.Router();
const AC = require("../controllers/authentication"); // authentication controller

router.post("/signUp", AC.create);
router.post("/login", AC.login);
router.post("/change-password", AC.changePassword);
router.post("/forgot-password", AC.forgotPassword);
router.post("/verify-token", AC.verifyToken);

module.exports = router;