// Innow8 Apps mailing credentials
module.exports.Innow8 = {
    service: "gmail",
    name: "Innow8 Apps",
    email: "innow82019@gmail.com",
    password: "Innow8@321",
    ceo: {
        name: "Chhavi Jindal",
        email: "cjindal@innow8apps.com",
    },
    cto: {
        name: "Kapil Bindal",
        email: "kbindal@innow8apps.com",
    },
    hr: {
        name: "HR",
        email: "hr@innow8apps.com",
    },
    employmentWebsite: "http://shrouded-tor-24090.herokuapp.com",
    team: "Innow8 Team",
    address: "605, Bestech Business Tower, Sector 66, Mohali (Punjab)"
};

// System type local/live
const sysTyp = "live";

// url
module.exports.frontendURL =
    sysTyp === "local"
        ? "http://localhost:3000"
        : "http://shrouded-tor-24090.herokuapp.com";
module.exports.backendURL =
    sysTyp === "local" ? "http://localhost:8000" : "http://3.7.30.2";

// Building the connection string
module.exports.dbUri =
    sysTyp === "local"
        ? "mongodb://localhost:27017/attendanceManagement"
        : "mongodb+srv://sunil:sunil@123@cluster0-z73f7.mongodb.net/test?retryWrites=true&w=majority";

// token expiry time
module.exports.expiryTime = {
    forgotPwd: "2h",
    login: "8h"
}

// allowed leaves per month
module.exports.leavesPerMonth = 1.25;

// leaves for first joining month
module.exports.firstMonthLeaves = (date) => {
    if (date) {
        if (1 <= date && date <= 15) {
            return 1.25;
        } else if (16 <= date && date <= 25) {
            return 0.5;
        } else if (26 <= date && date <= 27) {
            return 0.25;
        } else {
            return 0;
        }
    } else return 0;
}

// holiday working time value
module.exports.holidayWorkingTimeValue = hrs => {
    if (hrs) {
        if (hrs < 5) {
            return 0;
        } else if (hrs < 7) {
            return 0.5;
        } else if (hrs < 9) {
            return 0.75;
        } else if (hrs >= 9) {
            return 1;
        } else {
            return 0;
        }
    } else return 0;
}

// Absent time value
module.exports.absentTimeValue = hrs => {
    if (hrs) {
        if (6 <= hrs && hrs < 8) {
            return 0.25;
        } else if (4 <= hrs && hrs < 6) {
            return 0.5;
        } else if (hrs < 4) {
            return 1;
        } else {
            return 0;
        }
    } else return 0;
}

// employee first time password length
module.exports.empPwdLen = 15;

// send-grid key for mailing
module.exports.sendGridKey = "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";

// employee id format
module.exports.empIDFormat = {
    startsWith: "IN", // starting characters
    len: 6            // total id length
};

// load last days notifications
module.exports.notificationDaysLimit = 7;