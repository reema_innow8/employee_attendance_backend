const errors = require("../common/errors");

const indexes = arr => {
    try {
        let list = [];
        arr.forEach(element => {
            const keys = Object.keys(element);
            if (keys && keys.length > 0) {
                const subList = [];
                keys.forEach(ele => {
                    subList.push(`${ele}_${element[ele]}`);
                });
                const str = subList.join('_');
                list.push(str);
            }
        });
        return list;
    } catch (error) {
        console.log(`${errors.tryCatch}indexes`, error);
    }
}

const modelIndexes = model => {
    try {
        switch (model) {
            case 'users':
                return [{ email: 1, isDeleted: 1 }, { isDeleted: 1, admin: 1 }];
            case 'attendances':
                return [{ isDeleted: 1, createdAt: 1 }, { isDeleted: 1, createdAt: 1, employee: 1 }];
            case 'holidays':
                return [{ date: 1 }];
            case 'leaves':
                return [{ isDeleted: 1, employee: 1 }, { isDeleted: 1 }];
            case 'notifications':
                return [{ user: 1, createdAt: 1 }];
            case 'salaries':
                return [{ isDeleted: 1, employee: 1 }, { isDeleted: 1 }];
            default:
                return [];
        }
    } catch (error) {
        console.log(`${errors.tryCatch}modelIndexes`, error);
    }
}

const createDropIndexes = async obj => {
    try {
        const { db, model } = obj;
        const newIndexes = modelIndexes(model);
        const indexNames = indexes(newIndexes);
        const collection = db.collection(model);
        if (!collection) return;
        const oldIndexes = await collection.getIndexes();
        const oldIndexesKeys = Object.keys(oldIndexes);
        const dropIndexes = oldIndexesKeys.filter(ind => ind !== '_id_' && !indexNames.includes(ind));
        if (dropIndexes && dropIndexes.length > 0) await collection.dropIndex(dropIndexes);
        for (let i = 0; i < newIndexes.length; i++) {
            if (!oldIndexesKeys.includes(indexNames[i])) {
                await collection.createIndex(newIndexes[i]);
            }
        }
    } catch (error) {
        console.log(`${errors.tryCatch}${obj.model}createDropIndexes`, error);
    }
}

module.exports.createIndex = async db => {
    try {
        const modelList = ['holidays', 'users', 'attendances', 'leaves', 'notifications', 'salaries'];
        for (let model of modelList) {
            await createDropIndexes({ db, model });
        }
    } catch (error) {
        console.log(`${errors.tryCatch}createIndex`, error);
    }
}