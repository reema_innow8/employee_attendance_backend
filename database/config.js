"use strict";
const credentials = require("../credentials");

// Bring Mongoose into the app
const mongoose = require("mongoose");

// Created the database connection
mongoose.connect(
  credentials.dbUri,
  { useNewUrlParser: true }
);
mongoose.set("useCreateIndex", true);
mongoose.set("useFindAndModify", false);
mongoose.Promise = global.Promise;
let db = mongoose.connection;

// CONNECTION EVENTS
// When successfully connected
db.on("connected", () => {
  console.log("Successfully connected to the database");
});

// If any error occured in database connection
db.on("error", console.error.bind(console, "MongoDB connection error:"));

require("./modelIndexes").createIndex(db);
