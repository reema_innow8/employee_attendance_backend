"use strict";
const express = require("express");
const app = express();
const bodyParser = require('body-parser');
require("./database/config");
const port = process.env.PORT || "8000";
const logger = require('morgan');

// display api requests
app.use(logger("dev"));

const cors = require('cors');
app.use(cors());

const multer = require('multer');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/");
  },
  filename: (req, file, cb) => {
    cb(null, "attendance.txt")
  }
});

module.exports.upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "text/plain") {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error('Only .txt format allowed!'));
    }
  }
});

app.use(
  bodyParser.urlencoded({
    // Middleware
    extended: true,
  })
);

app.use(bodyParser.json());

// Authentication
app.use(require("./common/middleware").checkToken);

// Routes
require("./routes/index")(app);

app.listen(port, () => {
  console.log(`Listening to requests on ${require("./credentials").backendURL}`);
});