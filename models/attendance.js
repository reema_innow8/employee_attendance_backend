"use strict";
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

// attendance schema fields
let AttendanceSchema = new Schema({
  employee: { type: Schema.Types.ObjectId, ref: "User", required: true }, // employee
  entry: { type: Date },                                                  // employee in/entry date
  exit: { type: Date },                                                   // employee out/exit date
  createdAt: { type: Date, required: true },                              // employee in/out created date
  isDeleted: { type: Boolean, default: false },                           // entry deleted or not
});

// Apply the uniqueValidator plugin to add unique constraints in this schema.
AttendanceSchema.plugin(uniqueValidator);

// Export the model and  we need to create a model using it
module.exports = mongoose.model("Attendance", AttendanceSchema);
