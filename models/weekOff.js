"use strict"
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

// week-off schema fields
let WeekOffSchema = new Schema({
    // week days
    sunday: { type: Boolean, default: false },
    monday: { type: Boolean, default: false },
    tuesday: { type: Boolean, default: false },
    wednesday: { type: Boolean, default: false },
    thursday: { type: Boolean, default: false },
    friday: { type: Boolean, default: false },
    saturday: { type: Boolean, default: false }
});

// Apply the uniqueValidator plugin to add unique constraints in this schema.
WeekOffSchema.plugin(uniqueValidator);

// Export the model and  we need to create a model using it
module.exports = mongoose.model("WeekOff", WeekOffSchema);
