"use strict";
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

// salary schema fields
let SalarySchema = new Schema({
    employee: { type: Schema.Types.ObjectId, ref: "User", required: true }, // employee
    salary: { type: Number, required: true },                               // employee salary amount
    isDeleted: { type: Boolean, default: false }                            // employee salary deleted or not
},
    { timestamps: true }                                                    // created and updated dates
);

// Apply the uniqueValidator plugin to add unique constraints in this schema.
SalarySchema.plugin(uniqueValidator);

// Export the model and  we need to create a model using it
module.exports = mongoose.model("Salary", SalarySchema);
