"use strict"
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

// user schema fields
let UserSchema = new Schema({
  empId: { type: String, trim: true },                                     // id initiated by company
  name: { type: String, required: true, trim: true },                      // name
  email: { type: String, required: true, trim: true, lowercase: 1 },       // email
  contact: { type: String, required: true, trim: true },                   // contact
  password: { type: String, required: true },                              // password
  type: { type: String, required: true, trim: true, lowercase: 1 },        // user type (employee/admin)
  admin: { type: Schema.Types.ObjectId, ref: "User" },                     // user admin
  isDeleted: { type: Boolean, default: false },                            // account deleted or not
  token: { type: String },                                                 // forgot password token
  leaves: [{ count: Number, date: String }],                               // total leaves
  timeZone: { type: String, required: true },                              // user time zone
  joiningDate: { type: Date },                                             // joining date
  leaveBalance: [{
    count: { balance: Number, extra: Number, lossOfPay: Number, leave: Number },
    date: String
  }],                                                                      // total absent balance by month and year
},
  { timestamps: true }                                                     // created and updated dates
);

// Apply the uniqueValidator plugin to add unique constraints in this schema.
UserSchema.plugin(uniqueValidator);

// Export the model and  we need to create a model using it
module.exports = mongoose.model("User", UserSchema);
// module.exports.UserSchema = UserSchema;
