"use strict";
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

// holiday schema fields
let HolidaySchema = new Schema({
    title: { type: String, required: true, trim: true }, // event name
    date: { type: Date, required: true, unique: true }   // event date
});

// Apply the uniqueValidator plugin to add unique constraints in this schema.
HolidaySchema.plugin(uniqueValidator);

// Export the model and  we need to create a model using it
module.exports = mongoose.model("Holiday", HolidaySchema);
