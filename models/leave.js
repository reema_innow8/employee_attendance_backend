"use strict";
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

// leave schema fields
let LeaveSchema = new Schema({
    employee: { type: Schema.Types.ObjectId, ref: "User", required: true },  // employee
    subject: { type: String, required: true, trim: true },                   // subject
    message: { type: String, required: true, trim: true },                   // content
    type: { type: String, default: 'full_day', required: true, trim: true }, // type of leave (full/half/short) day
    dates: { type: [Date], required: true },                                 // list of leave dates
    status: { type: String, required: true },                                // status (sent/approved/rejected/cancelled)
    reason: { type: String, trim: true, trim: true },                        // approved/rejected reason
    isDeleted: { type: Boolean, default: false },                            // leave deleted or not
    isCancel: { type: Boolean, default: false }                              // leave cancellation request sent or not
},
    { timestamps: true }                                                     // created and updated dates 
);

// Apply the uniqueValidator plugin to add unique constraints in this schema.
LeaveSchema.plugin(uniqueValidator);

// Export the model and  we need to create a model using it
module.exports = mongoose.model("Leave", LeaveSchema);
