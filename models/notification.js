"use strict";
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

// notification schema fields
let notificationSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: "User", required: true },      // user (admin/employee)
    message: { type: String, required: true, trim: true },                   // content
    isRead: { type: Boolean, default: false, required: true },               // is read or not
    createdAt: { type: Date, default: Date.now, required: true },            // creation date
    type: { type: String, required: true },                                  // type (leave/user)
    leave: { type: Schema.Types.ObjectId, ref: "Leave" }                     // type reference
});

// Apply the uniqueValidator plugin to add unique constraints in this schema.
notificationSchema.plugin(uniqueValidator);

// Export the model and  we need to create a model using it
module.exports = mongoose.model("Notification", notificationSchema);
