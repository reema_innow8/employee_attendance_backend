"use strict";
const jwt = require("jsonwebtoken");
const secret = "attendance_management";
const errors = require("../common/errors");
const User = require("../models/user");
const responses = require("../common/responses");
const credentials = require("../credentials");
const expiryTime = credentials.expiryTime;

/**
 * @method expiryHours: This method check expiry time for specific work
 * @param {Object} data mail regarding data from previous/parent api method
 */
const expiryHours = type => {
  try {
    switch (type) {
      case "forgotPassword":
        return expiryTime.forgotPwd;
      case "login":
        return expiryTime.login;
    }
  }
  catch (err) {
    console.log(`${errors.tryCatch}expiryHours`, err);
    return "";
  }
}

/**
 * @method isValid: This method check expiry time for specific work
 * @param {Object} data mail regarding data from previous/parent api method
 */
const isValid = async data => {
  let result = true;
  try {
    if (data && data.email) {
      await User.findOne({ $and: [{ email: data.email }, { isDeleted: false }] }).lean()
        .then(result => {
          if (!result) result = false;
        }).catch(() => {
          result = false;
        });
    }
    return result;
  } catch (err) {
    console.log(`${errors.tryCatch}isValid`, err);
    return result;
  }
};

/**
 * @method checkToken: This middleware method use to check user accessibility and credentials
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 * @param {Object} next calls to api method after ckecking user accessibility
 */
module.exports.checkToken = async (req, res, next) => {
  try {
    let token = req.headers["x-access-token"] || req.headers["authorization"]; // Express headers are auto converted to lowercase
    let operation = req.headers["operation"];
    if (token || operation) {
      if (operation === "public") next();
      else {
        if (token && token.startsWith("Bearer ")) {
          // Remove Bearer from string
          token = token.slice(7, token.length);
        }
        if (token) {
          const isValidToken = await verifyToken(token, "middleware");
          if (isValidToken === false) return res.json(responses.authFailed);
          else next();
        } else return res.json(responses.notSupplyToken);
      }
    } else return res.json(responses.notAuthorized);
  } catch (err) {
    console.log(`${errors.tryCatch}checkToken`, err);
    return res.json(responses.error);
  }
};

/**
 * @method getToken: This method to get token using user acc details
 * @param {Object} email mail regarding data from previous/parent api method
 * @param {String} type expiry time for specific work from previous/parent api method
 */
module.exports.getToken = (email, type) => {
  let token = "";
  try {
    if (type) {
      token = jwt.sign({ email }, secret, {
        expiresIn: expiryHours(type) // expires in specific hours
      });
    } else {
      token = jwt.sign({ email }, secret);
    }
    return token;
  } catch (err) {
    console.log(`${errors.tryCatch}getToken`, err);
    return token;
  }
};

/**
 * @method verifyToken: This method use to verify token
 * @param {String} token token from previous/parent api method
 * @param {String} type calling from middleware or any other previous/parent method
 */
const verifyToken = async (token, type) => {
  let result = true;
  try {
    await jwt.verify(token, secret, async (err, decoded) => {
      if (type && type === "middleware") {
        const isDeleted = await isValid(decoded);
        if (isDeleted === false) result = false;
      }
      if (err) result = false;
    });
    return result;
  } catch (err) {
    console.log(`${errors.tryCatch}verifyToken`, err);
    return result;
  }
};

module.exports.verifyToken = verifyToken;