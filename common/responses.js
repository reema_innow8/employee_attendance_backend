// api's status
const success = 200;   // success
const failure = 400;   // failure
const already = 304;   // already have
const auth = 401;      // authentication failure

module.exports.error = {
  status: failure,
  message: "Something went wrong"
};
module.exports.login = {
  status: success,
  message: "Login Successfully."
};
module.exports.notExist = {
  status: failure,
  message: "User does not exists."
};
module.exports.wrongPassword = {
  status: failure,
  message: "Wrong password."
};
module.exports.contactAlready = {
  message: "Contact already used.",
  status: already
}
module.exports.emailAlready = {
  message: "Email already registered.",
  status: already
};
module.exports.idAlready = {
  message: "Employee ID already used.",
  status: already
};
module.exports.empSuccess = type => ({
  status: success,
  message: `Employee ${type} Successfully.`
});
module.exports.fetchSuccess = {
  message: "Fetch Successfully.",
  status: success,
}
module.exports.noRecord = type => ({
  status: failure,
  message: `No ${type}.`
});
module.exports.deletedAlready = {
  status: success,
  message: `Employee already deleted.`
};
module.exports.authFailed = {
  status: auth,
  message: "Login session expired."
};
module.exports.notSupplyToken = {
  status: auth,
  message: "Auth token is not supplied."
};
module.exports.notAuthorized = {
  status: auth,
  message: "You are not authorize to access mom api's."
};
module.exports.notPassword = {
  status: failure,
  message: "You don't have password."
}
module.exports.wrongOldPwd = {
  status: failure,
  message: "Old password is wrong."
}
module.exports.notMatchPwd = {
  status: failure,
  message: "New and Confirm passwords are not same."
}
module.exports.changePwdSuccess = {
  status: success,
  message: "Change Password Successfully."
}
module.exports.diffPwd = {
  status: failure,
  message: "Your old and new password are same."
}
module.exports.holidaySuccess = type => ({
  status: success,
  message: `Holiday ${type} Successfully.`
})
module.exports.attendanceSuccess = {
  status: success,
  message: "Employees attendance stored Successfully."
}
module.exports.leaveSuccess = type => ({
  status: success,
  message: `Leave ${type} Successfully.`
})
module.exports.leaveAlready = {
  status: already,
  message: "Leave already cancelled."
}
module.exports.wrongInput = type => ({
  status: failure,
  message: `Wrong ${type}.`
})
module.exports.updateSuccess = type => ({
  status: success,
  message: `${type} update Successfully.`
});
module.exports.salarySuccess = type => ({
  status: success,
  message: `Salary ${type} Successfully.`
})
module.exports.salaryAlready = {
  status: already,
  message: `Salary already added.`
}
module.exports.missingFile = {
  status: failure,
  message: "File is missing."
}
module.exports.mailSuccess = {
  status: success,
  message: "Mail sent Successfully."
}
module.exports.expireToken = {
  status: failure,
  message: "Token has been expired."
}
module.exports.mailFailure = {
  status: failure,
  message: "Mail sender is not working."
}
module.exports.alreadyHoliday = holiday => ({
  status: 400,
  message: `Already have ${holiday} holiday on this date.`
})
module.exports.invalidInput = type => ({
  status: 400,
  message: `Invalid ${type}.`
})
module.exports.weekOffSuccess = type => ({
  status: success,
  message: `Week-Off ${type} Successfully.`
})
module.exports.uploadSuccess = type => ({
  status: success,
  message: `${type} upload Successfully.`
})