"use strict";
// const nodemailer = require("nodemailer");
const errors = require("../common/errors");
const credentials = require("../credentials");
// const Innow8 = credentials.Innow8;
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(credentials.sendGridKey);

/**
 * @method Mailer: This method use to send email
 * @param {String} data mail regarding data from previous/parent api method
 */
module.exports.Mailer = data => {
    try {
        const { to, from, cc, bcc, subject, html } = data;
        const options = {
            to, cc, bcc, from, subject, html
        };
        sgMail.send(options);
    } catch (err) {
        console.log(`${errors.tryCatch}Mailer`, err);
    }
}

// using nodemailer
// module.exports.Mailer = async data => {
//     let result = false;
//     try {
//         const { to, from, cc, bcc, subject, html } = data;
//         const transporter = nodemailer.createTransport({
//             service: Innow8.service,
//             host: "smtp.gmail.com",
//             port: 587,
//             secure: false, // true for 465, false for other ports
//             auth: {
//                 user: Innow8.email,
//                 pass: Innow8.password
//             }
//         });
//         const options = {
//             to, cc, bcc, from, subject, html
//         };
//         // result = await transporter.sendMail(options, (err, info) => {
//         //     if (err) {
//         //         console.log(`${errors.query}Mailer`, err);
//         //         return false;
//         //     }
//         //     else {
//         //         transporter.close();
//         //         return true;
//         //     }
//         // });
//         result = await transporter.sendMail(options);
//         transporter.close();
//         return result;
//     } catch (err) {
//         console.log(`${errors.tryCatch}Mailer`, err);
//         return result;
//     }
// }
