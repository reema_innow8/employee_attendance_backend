"use strict";
const bcrypt = require("bcrypt");
const errors = require("../common/errors");
const pwdGenerator = require('generate-password');
const credentials = require('../credentials');

/**
 * @method generate: This method use to get encryption password
 * @param {String} pwd password from previous/parent api method
 */
module.exports.generate = async pwd => {
  try {
    const hash = await bcrypt.hash(pwd, 10)
    return hash;
  } catch (error) {
    console.log(`${errors.tryCatch}generate`, error);
    return "";
  }
}

/**
 * @method compare: This method use to verify password
 * @param {String} userPwd entered password from previous/parent api method
 * @param {String} dbPwd stored password in database from previous/parent api method
 */
module.exports.compare = async (userPwd, dbPwd) => {
  let result = false;
  try {
    const result = await bcrypt.compare(userPwd, dbPwd);
    return result;
  } catch (error) {
    console.log(`${errors.tryCatch}compare`, error);
    return result;
  }
}

/**
 * @method pwdCreator: This method use to create random password
 */
module.exports.pwdCreator = () => {
  let password = "";
  try {
    password = pwdGenerator.generate({
      length: credentials.empPwdLen,
      numbers: true,
      symbols: true,
      lowercase: true,
      uppercase: true,
      excludeSimilarCharacters: true,
      strict: true
    });
    return password;
  } catch (error) {
    console.log(`${errors.tryCatch}pwdCreator`, error);
    return password;
  }
}
