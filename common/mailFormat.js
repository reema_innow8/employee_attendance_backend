const credentials = require("../credentials");
const Innow8 = credentials.Innow8;
const errors = require("../common/errors");
const salaryCtr = require('../controllers/salary');

/**
 * @method emailFooter: This method use to create email template foot format
 */
const emailFooter = () => {
    try {
        const footer = `<strong>Thanks and Regards</strong>,
                        <br/>
                       ${credentials.Innow8.team}
                        <br/>
                        ${credentials.Innow8.address}`
        return footer;
    } catch (error) {
        console.log(`${errors.tryCatch}emailFooter`, err);
        return '';
    };
}

/**
 * @method pwdContent: This method use to create employee first password mail content
 * @param {Object} user mail regarding data from previous/parent api method
 */
module.exports.pwdContent = user => {
    let content = { subject: "", body: "" };
    try {
        content.subject = `An ${Innow8.name} administrator created an account for you at ${Innow8.employmentWebsite}`;
        content.body = `<body>
                Hi <strong>${salaryCtr.titleCase(user.name)},</strong>
                <br/>
                Welcome Onboard!!
                <br/>
                Please check your username and password below:
                <br/><br/>
                <strong>Username/Email</strong>: ${user.email}
                <br/>
                <strong>Password</strong>: ${user.password}
                <br/><br/>
                ${emailFooter()}
            </body>`
        return content;
    } catch (error) {
        console.log(`${errors.tryCatch}pwdContent`, err);
        return content;
    };
}

/**
 * @method fgtPwdContent: This method use to create forgot password mail content
 * @param {Object} obj link for set new password and user detail from previous/parent api method
 */
module.exports.fgtPwdContent = obj => {
    let content = { subject: "", body: "" };
    try {
        const { url, user } = obj;
        content.subject = "Forgot Password";
        content.body = `<body>
              <h2>Reset your password</h2>
              Hi <strong>${salaryCtr.titleCase(user.name)},</strong>
              <br/>
              You told us you forgot your password. If you really did, click here to choose a new one:
              <br/>
              <a href=${url}>
                Click Here
              </a>
              <br/>
              If you didn’t mean to reset your password, then you can just ignore this email; your password will not change.
              <br/><br/>
              ${emailFooter()}
          </body>`
        return content;
    } catch (error) {
        console.log(`${errors.tryCatch}fgtPwdContent`, err);
        return content;
    };
}

/**
 * @method leaveReplyContent: This method use to create leave reply mail content
 * @param {Object} leave mail regarding data from previous/parent api method
 */
module.exports.leaveReplyContent = leave => {
    let content = { subject: "", body: "" };
    try {
        const { reason, status } = leave;
        content.subject = `Re: ${leave.subject}`;
        content.body = `
                <head>
                    ${salaryCtr.titleCase(status)}.
                </head>
                 <br/>
                ${reason
                ? `<body><strong>Reason:- </strong>${reason}</body>`
                : ""}
                <br/>
                ${emailFooter()}
            `
        return content;
    } catch (error) {
        console.log(`${errors.tryCatch} leaveReplyContent`, err);
        return content;
    };
}