"use strict";
const errors = require("../common/errors");
const responses = require("../common/responses");

/**
 * @method validateEmail: This method use to validate the mail id
 * @param {String} pwd password from previous/parent api method
 */
const validateEmail = email => {
  try {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  } catch (err) {
    console.log(`${errors.tryCatch}validateRequiredKeys`, err);
    return false;
  }
}

/**
 * @method validatePassword: This method use to validate the password
 * @param {String} pwd password from previous/parent api method
 */
const validatePassword = password => {
  let result = false;
  try {
    if (password.length >= 6) result = true;
    return result;
  } catch (err) {
    console.log(`${errors.tryCatch}validateRequiredKeys`, err);
    return result;
  }
}

/**
 * @method validateRequiredKeys: This helping function is for validating a request body with required fields
 * @param {Object} body request from previous/parent api method
 * @param {Object} fields required fields from previous/parent api method
 * @param {Object} res response to previous/parent api method
 * @param {Object} callback It returns callback function containing one variable is response, error message if any required key is missing or empty
 */
exports.validateRequiredKeys = (body, fields, res, callback) => {
  try {
    let key = "";
    let name = "";
    let errorField = "";
    let value = "";
    // Traversing through the required fields
    for (let i = 0; i < fields.length; i += 1) {
      key = fields[i].key;
      name = fields[i].name;
      errorField = "";
      value = body[key] + "";
      // if request body does not contain respective field then throw error
      if (
        value === undefined ||
        value === "undefined" ||
        value === null ||
        !value ||
        value.replace(/\s+/g, "") === ""
      ) {
        if (key === "token") {
          errorField = responses.invalidInput("Link").message;
        } else {
          errorField = `${name} is required.`;
        }
        break;
      } else if (key === "email" && !validateEmail(value)) {
        errorField = "Wrong email address.";
        break;
      } else if (
        (key === "password" ||
          key === "confirm" ||
          key === "new" ||
          key === "old") &&
        !validatePassword(value)
      ) {
        errorField = "Password must be 6 characters long...";
        break;
      }
    }
    if (body.dates) {
      const dates = body.dates;
      if (!Array.isArray(dates)) {
        errorField = `${name} must be an array.`;
      }
      if (dates.length <= 0) {
        errorField = `${name} is required.`;
      }
    }
    const startDate = body.from || "";
    const endDate = body.to || "";
    if (startDate && endDate && new Date(startDate) > new Date(endDate)) {
      errorField = "End date should be greater than or equal to start date.";
    }
    if (errorField) {
      return res.json({
        status: 400,
        message: errorField
      });
    } else callback(errorField);
  } catch (err) {
    console.log(`${errors.tryCatch}validateRequiredKeys`, err);
    return res.json(err);
  }
};
