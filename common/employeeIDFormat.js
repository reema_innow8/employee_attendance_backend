const empIDFormat = require('../credentials').empIDFormat;

/**
 * @method checkEmpID: This method use to check employee ID format
 * @param {String} id employee id from previous/parent api method
 */
module.exports.checkEmpID = id => {
    let isValid = false;
    try {
        const len = id.length;
        if (len === empIDFormat.len && id.startsWith(empIDFormat.startsWith)) {
            isValid = true;
        }
        return isValid;
    } catch (err) {
        console.log(`${errors.tryCatch}checkEmpID`, err);
        return isValid;
    }
}