"use strict";
const util = require("../../common/utils");
const responses = require("../../common/responses");
const errors = require("../../common/errors");
const Salary = require('../../models/salary');
const User = require('../../models/user');
const Attendance = require('../../models/attendance');
const Holiday = require('../../models/holiday');
const WeekOff = require('../../models/weekOff');
const mongoose = require('mongoose');
const ObjectID = mongoose.Types.ObjectId;
const ExcelJS = require('exceljs');
const path = require('path');
const nodeXlsx = require('node-xlsx');
const moment = require('moment-timezone');
const credentials = require('../../credentials');

/**
 * @method add: This method use to add salary for any specific employee
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.add = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: "id", name: "Employee company ID" },
            { key: "salary", name: "Salary amount" }
        ],
        res,
        error => {
            try {
                if (!error) {
                    const query = User.findOne({
                        $and:
                            [
                                { empId: req.body.id },
                                { isDeleted: false }
                            ]
                    }).lean();
                    query.exec(async (err, emp) => {
                        if (err) {
                            console.log(`${errors.query}add`, err);
                            return res.json(responses.error);
                        }
                        if (emp) {
                            const salary = await Salary.findOne({ employee: ObjectID(emp._id) }).lean();
                            if (salary) return res.json(responses.salaryAlready);
                            const obj = {
                                employee: emp._id,
                                salary: req.body.salary
                            }
                            new Salary(obj).save().then(result => {
                                if (result) return res.json(responses.salarySuccess("added"));
                                else return res.json(responses.error);
                            });
                        } else return res.json(responses.wrongInput("ID"));
                    })
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}add`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method edit: This method use to edit salary for any specific employee
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.edit = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: "id", name: "Salary ID" },
            { key: "salary", name: "Salary amount" }
        ],
        res,
        error => {
            try {
                if (!error) {
                    const cond = {
                        $and: [
                            { _id: ObjectID(req.body.id) }, { isDeleted: false }
                        ]
                    };
                    const query = Salary.findOne(cond).lean();
                    query.exec(async (err, salary) => {
                        if (err) {
                            console.log(`${errors.query}edit`, err);
                            return res.json(responses.error);
                        }
                        if (salary) {
                            await Salary.updateOne(cond, { $set: { salary: req.body.salary } }).then(result => {
                                if (result) return res.json(responses.salarySuccess("updated"));
                                else return res.json(responses.error);
                            });
                        } else return res.json(responses.wrongInput("ID"));
                    })
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}edit`, error);
                return res.json(responses.error);
            }
        }
    );
};


/**
 * @method fetch: This method use to fetch all employee salaries
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.fetch = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: "id", name: "Data ID" },
            { key: "limit", name: "Limit" },
            { key: "offset", name: "Offset" }
        ],
        res,
        async error => {
            try {
                if (!error) {
                    const { search, limit, offset } = req.body;
                    const cond = [{ isDeleted: false }];
                    const user = await User.findOne({ _id: ObjectID(req.body.id) }).lean();
                    if (!user) return res.json(responses.wrongInput("ID"));
                    if (user.type && user.type !== "admin") cond.push({ employee: ObjectID(user._id) });
                    const empCond = { path: "employee", select: { _id: 0, empId: 1, name: 1 } };
                    const text = search && search.trim() || "";
                    if (text) {
                        empCond["match"] = { $or: [{ name: { $regex: text, $options: "i" } }, { empId: { $regex: text, $options: "i" } }] };
                    }
                    const query = Salary.find({ $and: cond })
                        // .limit(limit)
                        // .skip(offset)
                        .populate(empCond).lean();
                    query.exec(async (err, salaries) => {
                        if (err) {
                            console.log(`${errors.query}fetch`, err);
                            return res.json(responses.error);
                        }
                        if (salaries && salaries.length > 0) {
                            let filtered = salaries.filter(i => i.employee);
                            // const count = await Salary.find({ $and: cond }).countDocuments();
                            const data = {
                                salaries: filtered.slice(offset, offset + limit), count: filtered.length
                            }
                            return res.json({ ...responses.salarySuccess("fetch"), data });
                        }
                        else return res.json(responses.noRecord("Salary"));
                    });
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}fetch`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method leaveBal: This method use to calculate leave balance of employee
 * @param {Object} obj employee detail, selectedDate, curMonth and timeZone from previous/parent api method
 */
const leaveBal = async obj => {
    let bal = 0;
    try {
        const { employee, timeZone, prevMonthDate, balance, salariesMonth, extra, lop, absent } = obj;
        const { leaveBalance } = employee;
        if (leaveBalance && leaveBalance.length > 0 && salariesMonth === "Dec") {
            const firstDateOfCurYear = (timeZone ? moment().tz(timeZone).startOf('year') : moment().startOf('year')).format();
            for (let item of leaveBalance) {
                const date = (timeZone ? moment(item.date).tz(timeZone) : moment(item.date)).format();
                if (firstDateOfCurYear <= date) {
                    if (item.count.balance > 0) {
                        bal += item.count.balance;
                    }
                }
            }
        }
        const cond = { _id: ObjectID(employee._id) };
        const prevMonthYear = prevMonthDate.format("YYYY-MM");
        bal += balance;
        let index = -1;
        if (leaveBalance) {
            index = leaveBalance.findIndex(i => i.date === prevMonthYear);
        }
        const leaveObj = { balance, extra, lossOfPay: lop, leave: absent };
        let data = "";
        if (index < 0) {
            data = { $push: { leaveBalance: { count: leaveObj, date: prevMonthYear } } };
        } else {
            cond['leaveBalance._id'] = ObjectID(leaveBalance[index]._id);
            data = { $set: { "leaveBalance.$.count": leaveObj } };
        }
        await User.updateOne(cond, data);
        return bal;
    } catch (error) {
        console.log(`${errors.tryCatch}leaveBal`, error);
        return bal;
    }
};

/**
 * @method calculateHrsValue: This method use to calculate absent and extra working time val of employee
 * @param {Object} time contains days and hours from previous/parent api method
 * @param {Boolean} isHoliday Is holiday or not from previous/parent api method
 */
const calculateHrsValue = (time, isHoliday) => {
    const { days, hours } = time;
    let extra = 0, absent = 0;
    try {
        if (isHoliday === true) {
            extra = credentials.holidayWorkingTimeValue(hours);
        } else if (days <= 0) {
            absent = credentials.absentTimeValue(hours);
        }
        return { absent, extra };
    } catch (error) {
        console.log(`${errors.tryCatch}calculateHrsValue`, error);
        return { absent, extra };
    }
};

/**
 * @method dateDifference: This method use to calculate dates difference in terms of days, hours and mintues
 * @param {Object} startDate starting date from previous/parent api method
 * @param {Object} endDate ending date from previous/parent api method
 */
const dateDifference = (startDate, endDate) => {
    let result = {
        days: 0,
        hours: 0,
        minutes: 0,
    };
    try {
        if (!startDate || !endDate) return result;
        const ticks = (new Date(endDate) - new Date(startDate)) / 1000;
        let delta = Math.abs(ticks);
        // calculate (and subtract) whole days
        result.days = Math.floor(delta / 86400);
        delta -= result.days * 86400;
        // calculate (and subtract) whole hours
        result.hours = Math.floor(delta / 3600) % 24;
        delta -= result.hours * 3600;
        // calculate (and subtract) whole minutes
        result.minutes = Math.floor(delta / 60) % 60;
        delta -= result.minutes * 60;
        return result;
    } catch (error) {
        console.log(`${errors.tryCatch}dateDifference`, error);
        return result;
    }
};

/**
 * @method minHrDayConverter: This method use to convert lower units of date to upper units
 * @param {Object} time contains days, hours and mintues from previous/parent api method
 */
const minHrDayConverter = (time) => {
    try {
        let { days, hours, minutes } = time;
        if (minutes > 59) {
            hours += Math.floor(minutes / 60);
            minutes = minutes % 60;
        }
        if (hours > 24) {
            days += Math.floor(hours / 24);
            hours = hours % 24;
        }
        return { days, hours, minutes };
    } catch (error) {
        console.log(`${errors.tryCatch}minHrDayConverter`, error);
        return time;
    }
};

/**
 * @method checkJoiningDate: This method use to check employee joining date
 * @param {Object} employee employee detail from previous/parent api method
 * @param {Object} date date from previous/parent api method
 * @param {String} timeZone time-zone from previous/parent api method
 */
const checkJoiningDate = (employee, date, timeZone) => {
    let isJoined = true;
    try {
        const { joiningDate } = employee;
        if (joiningDate) {
            if (timeZone) {
                isJoined = moment(joiningDate).tz(timeZone).format() <= date.format();
            } else {
                isJoined = moment(joiningDate).format() <= date.format();
            }
        }
        return isJoined;
    } catch (error) {
        console.log(`${errors.tryCatch}checkJoiningDate`, error);
        return isJoined;
    }
}

/**
 * @method getDates: This method to get the compile salaries month dates
 * @param {Object} date start and end date of month from previous/parent api method
 * @param {String} timeZone admin time-zone from previous/parent api method
 */
const getDates = (date, timeZone) => {
    let arr = [];
    try {
        let { start, end } = date;
        if (!start || !end) return arr;
        if (timeZone) {
            for (; start <= end; start = moment(start).tz(timeZone).add(1, "days").format()) {
                arr.push(moment(start).tz(timeZone));
            }
        } else {
            for (; start <= end; start = moment(start).add(1, "days").format()) {
                arr.push(moment(start));
            }
        }
        return arr;
    } catch (error) {
        console.log(`${errors.tryCatch}getDates`, error);
        return arr;
    }
};

/**
 * @method titleCase: This method to get title case string
 * @param {String} str string from previous/parent api method
 */
const titleCase = str => {
    try {
        if (!str) return str;
        const newStr = str.charAt(0).toUpperCase() + str.slice(1);
        return newStr;
    } catch (error) {
        console.log(`${errors.tryCatch}titleCase`, error);
        return str;
    }
}

/**
 * @method effectiveTime: This method use to calculate working time of employee on specific date
 * @param {Object} data contains list of employee in and out entries from previous/parent api method
 * @param {Boolean} isHoliday Is holiday or not from previous/parent api method
 */
const effectiveTime = (data, isHoliday) => {
    let val = 0;
    try {
        let { entry, exit } = data;
        entry.sort((a, b) => new Date(a) - new Date(b));
        exit.sort((a, b) => new Date(a) - new Date(b));
        let days = 0,
            hours = 0,
            minutes = 0;
        for (let i = 0; i < entry.length; i++) {
            const startDate = entry[i] || "";
            const endDate = exit[i] || "";
            if (startDate && endDate) {
                let df = dateDifference(startDate, endDate);
                if (df.days) days += df.days;
                if (df.hours) hours += df.hours;
                if (df.minutes) minutes += df.minutes;
            }
        }
        const time = minHrDayConverter({ days, hours, minutes });
        val = calculateHrsValue(time, isHoliday);
        return val;
    } catch (error) {
        console.log(`${errors.tryCatch}effectiveTime`, error);
        return val;
    }
};

/**
 * @method checkHoliday: This method use to check holiday on specific date
 * @param {Object} obj contains public holidays, weekends and date from previous/parent api method
*/
const checkHoliday = (obj) => {
    let isHoliday = false;
    try {
        const { weekoffs, holidays, dateObj } = obj;
        const day = dateObj.date.format('dddd');
        isHoliday = weekoffs.includes(day) || holidays.includes(dateObj.onlyDate);
        return isHoliday;
    } catch (error) {
        console.log(`${errors.tryCatch}checkHoliday`, error);
        return isHoliday;
    }
};

/**
 * @method calculateHrs: This method use to calculate leaves and extra working time of employees
 * @param {Array} entries list of employee attendance entries from previous/parent api method
 * @param {Array} weekoffs list of weekends from previous/parent api method
 * @param {Array} holidays list of public holidays from previous/parent api method
 * @param {String} timeZone admin time-zone from previous/parent api method
 * @param {Array} dates list of compile salaries month dates from previous/parent api method
 */
const calculateHrs = (entries, weekoffs, holidays, timeZone, dates) => {
    let effHrs = [];
    try {
        if (entries && entries.length > 0) {
            for (let entry of entries) {
                if (
                    entry.employee && entry.employee.length > 0 && entry.employee[0]
                    && entry.list && entry.list.length > 0
                    && entry.salary && entry.salary.length > 0 && entry.salary[0]
                ) {
                    const obj = { employee: entry.employee[0], salary: entry.salary[0] };
                    let absent = 0;
                    let extra = 0;
                    let lop = 0;
                    for (let date of dates) {
                        const isJoined = checkJoiningDate(obj.employee, date, timeZone);
                        if (isJoined === true) {
                            let val = { extra: 0, absent: 1 };
                            const onlyDate = date.format("DD-MM-YYYY");
                            const index = entry.list.findIndex(i => i.date === onlyDate);
                            const holidayObj = {
                                weekoffs, holidays, dateObj: { date, onlyDate }
                            }
                            const isHoliday = checkHoliday(holidayObj);
                            if (index > -1) {
                                val = effectiveTime(entry.list[index], isHoliday);
                            } else if (isHoliday === true) {
                                val.absent = 0;
                            }
                            if (isHoliday === false && val.absent > 0) {
                                const index = entry.leaves.findIndex(i => {
                                    const ind = i.dates.findIndex(j => {
                                        const k = (timeZone ? moment(j).tz(timeZone) : moment(j)).format();
                                        if (k === date.format()) return true;
                                    })
                                    if (ind > -1) return true;
                                })
                                if (index < 0) lop += val.absent;
                                else absent += val.absent;
                            }
                            extra += val.extra;
                        }
                    }
                    effHrs.push({ ...obj, absent, extra, lop });
                }
            }
        }
        return effHrs;
    } catch (error) {
        console.log(`${errors.tryCatch}calculateHrs`, error);
        return effHrs;
    }
};

/**
 * @method monthEntries: This method use to fetch employee attendance entries, public holidays and week-offs
 * @param {Object} date compile salaries month date from previous/parent api method
 * @param {Array} empID list of employee id's from previous/parent api method
 * @param {String} timeZone admin time-zone from previous/parent api method
 */
const monthEntries = async (date, empID, timeZone) => {
    let effHrs = [];
    try {
        const dateCond = { $gte: new Date(date.start), $lte: new Date(date.end) };
        const cond = [{ isDeleted: false }, { createdAt: dateCond }, { employee: { $in: empID } }];
        const entries = await Attendance.aggregate([
            {
                $match: { $and: cond }
            },
            {
                $group:
                {
                    _id: {
                        _id: "$employee",
                        date: { $dateToString: { format: "%d-%m-%Y", date: "$createdAt" } },
                    },
                    entry: {
                        $addToSet: "$$ROOT.entry"
                    },
                    exit: {
                        $addToSet: "$$ROOT.exit"
                    }
                }
            },
            {
                $sort: { _id: 1 }
            },
            {
                $group:
                {
                    _id: "$_id._id",
                    list:
                    {
                        $push:
                        {
                            date: "$_id.date",
                            entry: "$entry",
                            exit: "$exit",
                        }
                    }
                }
            },
            {
                $lookup:
                {
                    from: "users",
                    localField: "_id",
                    foreignField: "_id",
                    as: "employee"
                }
            },
            {
                $lookup:
                {
                    from: "salaries",
                    localField: "_id",
                    foreignField: "employee",
                    as: "salary"
                }
            },
            {
                $lookup:
                {
                    from: "leaves",
                    as: "leaves",
                    let: { employee: "$_id" },
                    pipeline: [
                        {
                            $match:
                            {
                                $expr:
                                {
                                    $and:
                                        [
                                            { $eq: ["$$employee", "$employee"] },
                                            { $eq: ["$status", "approved"] },
                                        ]
                                }
                            }
                        },
                        { $match: { dates: { $elemMatch: dateCond } } }
                    ],
                }
            }
        ]);
        const { dates, holidays, weekoffs } = await holidayWeekend({ date, timeZone, dateCond });
        effHrs = calculateHrs(entries, weekoffs, holidays, timeZone, dates);
        return effHrs;
    } catch (error) {
        console.log(`${errors.tryCatch}monthEntries`, error);
        return effHrs;
    }
}

/**
 * @method holidayWeekend: This method use to fetch holidays, week-offs and dates
 * @param {Object} obj dates, time-zone and query condition from previous/parent api method
 */
const holidayWeekend = async obj => {
    let dates = [];
    const holidays = [], weekoffs = [];
    try {
        const { date, timeZone, dateCond } = obj;
        const holidayList = await Holiday.find({ date: dateCond }).select({ _id: 0, date: 1 }).lean();
        if (timeZone) {
            holidayList.forEach(i => {
                holidays.push(moment(i.date).tz(timeZone).format('DD-MM-YYYY'));
            });
            if (date) dates = getDates(date, timeZone);
        } else {
            holidayList.forEach(i => {
                holidays.push(moment(i.date).format('DD-MM-YY'));
            });
            if (date) dates = getDates(date);
        }
        const weekoffList = await WeekOff.findOne().select({ _id: 0, sunday: 1, monday: 1, tuesday: 1, wednesday: 1, thursday: 1, friday: 1, saturday: 1 }).lean();
        const weekDays = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
        weekDays.forEach(i => {
            if (weekoffList[i] === true) {
                weekoffs.push(titleCase(i));
            }
        });
        return { dates, holidays, weekoffs };
    } catch (error) {
        console.log(`${errors.tryCatch}holidayWeekend`, error);
        return { dates, holidays, weekoffs };
    }
}

/**
 * @method createExcelSheet: This method use to save compile salaries excel file
 * @param {Object} obj salaries, filePath, timeZone, salariesMonth from previous/parent api method
 */
const createExcelSheet = async obj => {
    const salList = [];
    try {
        const { salaries, filePath, timeZone, salariesMonth } = obj;
        let curDate, curYear, curMonth, prevMonthDate, selectedDate;
        if (timeZone) {
            curDate = moment().tz(timeZone).format();
            curYear = moment(curDate).tz(timeZone).format("YYYY");
            curMonth = moment(curDate).tz(timeZone).format("MM");
            prevMonthDate = moment(curDate).tz(timeZone).subtract(1, 'months');
            selectedDate = moment(`${curYear}-${curMonth}`).tz(timeZone).format();
        } else {
            curDate = moment().format();
            curYear = moment(curDate).format("YYYY");
            curMonth = moment(curDate).format("MM");
            prevMonthDate = moment(curDate).subtract(1, 'months');
            selectedDate = moment(`${curYear}-${curMonth}`).format();
        }
        const workbook = new ExcelJS.Workbook();
        const worksheet = workbook.addWorksheet(`${prevMonthDate.format("MMM-YYYY")}_salaries`);
        const columns = [
            { header: 'ID', key: 'id', width: 10 },
            { header: 'Emp ID', key: 'emp_id', width: 32 },
            { header: 'Employee', key: 'employee', width: 32 },
            { header: 'Salary', key: 'salary', width: 32 },
            { header: 'Loss of Pay(LOP)', key: 'lop', width: 32 },
            // { header: 'Extra Working Days', key: 'extra_working_days', width: 32 },
            { header: 'Leave Balance', key: 'leave_balance', width: 32 },
            { header: 'Final Salary', key: 'final_salary', width: 32 }
        ];
        worksheet.columns = columns;

        if (salaries) {
            let totalSalary = 0;
            let totalRemainingSalary = 0;
            let j = 0;
            for (let i = 0; i < salaries.length; i++) {
                const { employee, extra, lop, absent } = salaries[i];
                if (employee.joiningDate) {
                    const joiningDate = timeZone ? moment(employee.joiningDate).tz(timeZone) : moment(employee.joiningDate);
                    const joiningMonthYear = joiningDate.format("YYYY-MM");
                    if (!(joiningMonthYear === `${curYear}-${curMonth}`)) {
                        j++;
                        let { salary } = salaries[i].salary;
                        const salPerDay = salary / 30;
                        let balance = extra - absent;
                        const joiningMonthDate = parseInt(joiningDate.format("D"));
                        if (prevMonthDate.format("YYYY-MM") === joiningMonthYear) {
                            if (joiningMonthDate > 1) {
                                salary = salPerDay * (31 - joiningMonthDate);
                            }
                            balance += credentials.firstMonthLeaves(joiningMonthDate);
                        } else {
                            balance += credentials.leavesPerMonth;
                        }
                        totalSalary += salary;
                        const totalLeaveBal = await leaveBal({ ...salaries[i], timeZone, balance, prevMonthDate, salariesMonth });
                        let finalSalary = salary - salPerDay * lop;
                        if (salariesMonth === "Dec") {
                            finalSalary += salPerDay * totalLeaveBal;
                        } else if (totalLeaveBal < 0) {
                            finalSalary += salPerDay * totalLeaveBal;
                        }
                        totalRemainingSalary += finalSalary;
                        if (salary > 0) {
                            const obj = {
                                id: j,
                                emp_id: employee.empId,
                                employee: employee.name,
                                salary,
                                leave_balance: totalLeaveBal,
                                lop,
                                // extra_working_days: extra,
                                final_salary: finalSalary.toFixed(2)
                            };
                            worksheet.addRow(obj);
                            salList.push(obj);
                        }
                    }
                }
            }
            if (j > 0) {
                const obj = {
                    id: 'Total',
                    salary: totalSalary,
                    final_salary: totalRemainingSalary.toFixed(2)
                }
                worksheet.addRow(obj);
                salList.push(obj);
            }
        }
        await workbook.xlsx.writeFile(filePath);
        return salList;
    } catch (error) {
        console.log(`${errors.tryCatch}createExcelSheet`, error);
        return salList;
    }
}

/**
 * @method compile: This method use to fetch all employee salaries
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.compile = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [{ key: "id", name: "Admin ID" }],
        res,
        async error => {
            try {
                if (!error) {
                    const id = ObjectID(req.body.id);
                    const query = User.find({
                        $and: [
                            { isDeleted: false }, { admin: id }
                        ]
                    }).lean();
                    query.exec(async (err, emp) => {
                        if (err) {
                            console.log(`${errors.query}compile`, err);
                            return res.json(responses.error);
                        }
                        if (emp && emp.length > 0) {
                            const empID = [];
                            for (let i of emp) {
                                empID.push(ObjectID(i._id));
                            }
                            const user = await User.findOne({ _id: id }).select({ _id: 0, timeZone: 1 }).lean();
                            const { timeZone } = user;
                            const date = (timeZone ? moment().tz(timeZone) : moment()).subtract(1, 'months');
                            const salariesMonth = date.format("MMM");
                            let start = '', end = '';
                            if (timeZone) {
                                start = moment(date).tz(timeZone).startOf('month').format();
                                end = moment(date).tz(timeZone).endOf('month').format();
                            } else {
                                start = moment(date).startOf('month').format();
                                end = moment(date).endOf('month').format();
                            }
                            const entries = await monthEntries({ start, end }, empID, timeZone);
                            const fileName = "salary.xlsx";
                            const filePath = path.join(__dirname, `../../public/${fileName}`);
                            const salList = await createExcelSheet({ salaries: entries, filePath, timeZone, salariesMonth });
                            return res.json({ ...responses.salarySuccess('compile'), data: salList });
                        } else {
                            return res.json(responses.noRecord("Employee"));
                        }
                    });
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}compile`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method download: This method use to download excel-sheet for all employee salaries
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.download = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [],
        res,
        async error => {
            try {
                if (!error) {
                    let records = nodeXlsx.parse(`${__dirname}/../../public/salary.xlsx`);
                    const data = records && records.length > 0 ? records[0] : { name: "", data: [] };
                    return res.json({ ...responses.fetchSuccess, data });
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}download`, error);
                return res.json(responses.error);
            }
        }
    );
};

module.exports.holidayWeekend = holidayWeekend;
module.exports.checkHoliday = checkHoliday;
module.exports.titleCase = titleCase;
module.exports.dateDifference = dateDifference;
module.exports.getDates = getDates;
