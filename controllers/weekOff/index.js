"use strict";
const WeekOff = require("../../models/weekOff");
const util = require("../../common/utils");
const responses = require("../../common/responses");
const errors = require("../../common/errors");
const mongoose = require('mongoose');
const ObjectID = mongoose.Types.ObjectId;

/**
 * @method add: This method use to add week-offs of company
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.add = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [],
        res,
        error => {
            try {
                if (!error) {
                    const obj = {
                        monday: req.body.monday || false,
                        tuesday: req.body.tuesday || false,
                        wednesday: req.body.wednesday || false,
                        thursday: req.body.thursday || false,
                        friday: req.body.friday || false,
                        saturday: req.body.saturday || false,
                        sunday: req.body.sunday || false,
                    };
                    new WeekOff(obj).save().then(result => {
                        return res.json({ ...responses.weekOffSuccess("add"), data: result });
                    }).catch(err => {
                        console.log(`${errors.query}add`, err);
                        return res.json(responses.error);
                    })
                }
            } catch (error) {
                console.log(`${errors.tryCatch}add`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method edit: This method use to edit week-offs of company
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.edit = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [{ key: "id", name: "Week-Off ID" }],
        res,
        error => {
            try {
                if (!error) {
                    const cond = { _id: ObjectID(req.body.id) };
                    const query = WeekOff.findOne(cond).lean();
                    query.exec(async (err, weekOffs) => {
                        if (err) {
                            console.log(`${errors.query}edit`, err);
                            return res.json(responses.error);
                        }
                        if (weekOffs) {
                            const obj = {
                                monday: req.body.monday || false,
                                tuesday: req.body.tuesday || false,
                                wednesday: req.body.wednesday || false,
                                thursday: req.body.thursday || false,
                                friday: req.body.friday || false,
                                saturday: req.body.saturday || false,
                                sunday: req.body.sunday || false,
                            };
                            await WeekOff.updateOne(cond, { $set: obj }).then(async result => {
                                if (result) {
                                    const weekoffs = await WeekOff.findOne(cond).select({ sunday: 1, monday: 1, tuesday: 1, wednesday: 1, thursday: 1, friday: 1, saturday: 1 }).lean();
                                    return res.json({ ...responses.weekOffSuccess("updated"), data: weekoffs });
                                }
                                else return res.json(responses.error);
                            });
                        } else return res.json(responses.wrongInput("ID"));
                    })
                }
            } catch (error) {
                console.log(`${errors.tryCatch}edit`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method fetch: This method use to fetch all week-offs of company
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.fetch = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [],
        res,
        error => {
            try {
                if (!error) {
                    WeekOff.findOne()
                        .select({ sunday: 1, monday: 1, tuesday: 1, wednesday: 1, thursday: 1, friday: 1, saturday: 1 }).lean()
                        .then(result => {
                            return res.json({ ...responses.weekOffSuccess("fetch"), data: result });
                        }).catch(err => {
                            console.log(`${errors.query}fetch`, err);
                        });
                }
            } catch (error) {
                console.log(`${errors.tryCatch}fetch`, error);
                return res.json(responses.error);
            }
        }
    );
};
