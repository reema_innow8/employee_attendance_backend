"use strict";
const util = require("../../common/utils");
const mongoose = require("mongoose");
const ObjectID = mongoose.Types.ObjectId;
const responses = require("../../common/responses");
const errors = require("../../common/errors");
const Holiday = require('../../models/holiday');

/**
 * @method add: This method use to add new holiday
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.add = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: "title", name: "Title" },
            { key: "date", name: "Date" }
        ],
        res,
        async error => {
            try {
                if (!error) {
                    const { date } = req.body;
                    const holiday = await Holiday.findOne({ date: new Date(date) }).lean();
                    if (holiday) {
                        return res.json(responses.alreadyHoliday(holiday.title));
                    } else {
                        const obj = {
                            title: req.body.title, date
                        }
                        await new Holiday(obj).save().then(result => {
                            if (result) {
                                const data = {
                                    title: result.title,
                                    date: result.date
                                }
                                return res.json({ ...responses.holidaySuccess('added'), data });
                            } else return res.json(responses.error);
                        });
                    }
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}add`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method edit: This method use to update the holiday detail
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.edit = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: "id", name: "Holiday ID" },
            { key: "title", name: "Title" },
            { key: "date", name: "Date" }
        ],
        res,
        async error => {
            try {
                if (!error) {
                    const { id, date } = req.body;
                    const holiday = await Holiday.findOne({ $and: [{ date: new Date(date) }, { _id: { $ne: ObjectID(id) } }] }).lean();
                    if (holiday) {
                        return res.json(responses.alreadyHoliday(holiday.title));
                    } else {
                        const obj = {
                            title: req.body.title, date
                        }
                        await Holiday.updateOne({ _id: ObjectID(id) }, { $set: obj }).then(result => {
                            if (result) return res.json(responses.holidaySuccess('updated'));
                            else return res.json(responses.wrongInput("ID"));
                        });
                    }
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}edit`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method remove: This method use to delete existing holiday
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.remove = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [{ key: "id", name: "Holiday ID" }],
        res,
        error => {
            try {
                if (!error) {
                    const { id } = req.body;
                    const query = Holiday.deleteOne({
                        _id: ObjectID(id)
                    });
                    query.exec(async (err, holiday) => {
                        if (err) {
                            console.log(`${errors.query}remove`, err);
                            return res.json(responses.error);
                        }
                        if (holiday) return res.json(responses.holidaySuccess("deleted"));
                        else return res.json(responses.wrongInput("ID"));
                    });
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}remove`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method fetch: This method use to fetch all company holidays
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.fetch = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: "limit", name: "Limit" },
            { key: "offset", name: "Offset" }
        ],
        res,
        error => {
            try {
                if (!error) {
                    const query = Holiday.find()
                        .limit(req.body.limit)
                        .skip(req.body.offset)
                        .sort({ date: 1 }).lean();
                    query.exec(async (err, holidays) => {
                        if (err) {
                            console.log(`${errors.query}fetch`, err);
                            return res.json(responses.error);
                        }
                        if (holidays && holidays.length > 0) {
                            const count = await Holiday.find().countDocuments();
                            const data = {
                                holidays: JSON.parse(JSON.stringify(holidays)), count
                            }
                            return res.json({ ...responses.fetchSuccess, data });
                        }
                        else return res.json(responses.noRecord("Holiday"));
                    });
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}fetch`, error);
                return res.json(responses.error);
            }
        }
    );
};

