"use strict";
const User = require("../../models/user");
const WeekOff = require("../../models/weekOff");
const Holiday = require("../../models/holiday");
const util = require("../../common/utils");
const pwdCont = require("../../common/password");
const responses = require("../../common/responses");
const errors = require("../../common/errors");
const middleware = require("../../common/middleware");
const mongoose = require("mongoose");
const ObjectID = mongoose.Types.ObjectId;
const mailCtr = require("../../common/mailer");
const notificationCtr = require("../../controllers/notification");
const credentials = require("../../credentials");
const Innow8 = credentials.Innow8;
const moment = require("moment-timezone");
const mailFormat = require('../../common/mailFormat');

/**
 * @method getHolidays: This method use to get holidays
 * @param {Object} data data from previous/parent api method
 */
const getHolidays = async (data) => {
  let holidays = [];
  try {
    if (data) {
      let startDate, endDate;
      if (data.from && data.to) {
        startDate = new Date(data.from);
        endDate = new Date(data.to);
      } else {
        const { timeZone } = data;
        const curDate = new Date(timeZone ? moment().tz(timeZone).format() : moment().format());
        const curYear = curDate.getFullYear();
        const curMonth = curDate.getMonth();
        startDate = new Date(curYear, curMonth, 1);
        endDate = new Date(curYear, curMonth + 1, 0);
      }
      holidays = await Holiday.find({
        date: { $gte: startDate, $lte: endDate }
      }).select({ _id: 0, date: 1 }).lean();
    }
    return holidays;
  } catch (error) {
    console.log(`${errors.tryCatch}getHolidays`, error);
    return holidays;
  }
}

/**
 * @method create: This method use to add new account
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.create = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "name", name: "Name" },
      { key: "email", name: "Email" },
      { key: "contact", name: "Contact" },
      { key: "password", name: "Password" },
      { key: "timeZone", name: "Time Zone" }
    ],
    res,
    error => {
      try {
        if (!error) {
          const contact = req.body.contact.trim();
          const lowCaseEmail = req.body.email.trim().toLowerCase();
          const query = User.findOne({
            $and: [
              { $or: [{ email: lowCaseEmail }, { contact }] },
              { isDeleted: false }
            ]
          }).lean();
          query.exec(async (err, user) => {
            if (err) {
              console.log(`${errors.query}sign-up`, err);
              return res.json(responses.error);
            }
            if (user) {
              if (user.contact === contact) return res.json(responses.contactAlready);
              else if (user.email === lowCaseEmail) return res.json(responses.emailAlready);
              else return res.json(responses.idAlready);
            } else {
              // Function to get hashed password
              const hash = await pwdCont.generate(req.body.password);
              if (hash) {
                let user = new User({
                  name: req.body.name,
                  contact,
                  email: lowCaseEmail,
                  password: hash,
                  type: "admin",
                  timeZone: req.body.timeZone
                });
                // Store hashed password and other details of user in database
                user.save((err) => {
                  if (err) return res.json({ ...responses.error, error: err });
                  return res.json({
                    status: 200, message: "User created successfully."
                  });
                });
              }
            }
          });
        }
      } catch (error) {
        console.log(`${errors.tryCatch}sign-up`, error);
        return res.json(responses.error);
      }
    }
  );
};

/**
 * @method login: This method use to validate the user
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.login = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "email", name: "Email" },
      { key: "password", name: "Password" },
      { key: "timeZone", name: "Time Zone" }
    ],
    res,
    error => {
      try {
        if (!error) {
          const cond = {
            $and: [
              { email: req.body.email },
              { isDeleted: false }]
          };
          const query = User.findOne(cond).lean();
          query.exec(async (err, user) => {
            if (err) {
              console.log(`${errors.query}login`, err);
              return res.json(responses.error);
            }
            if (user) {
              const result = await pwdCont.compare(req.body.password, user.password);
              if (result === false) return res.json(responses.wrongPassword);
              else {
                const weekoffs = await WeekOff.findOne()
                  .select({ _id: 0, sunday: 1, monday: 1, tuesday: 1, wednesday: 1, thursday: 1, friday: 1, saturday: 1 }).lean();
                const holidays = await getHolidays({ timeZone: user.timeZone });
                const notifications = await notificationCtr.fetch(user);
                const userObj = {
                  id: user._id,
                  empId: user.empId,
                  name: user.name,
                  email: user.email,
                  contact: user.contact,
                  type: user.type
                };
                const authToken = middleware.getToken(user.email, "login");
                await User.updateOne(cond, { $set: { timeZone: req.body.timeZone } });
                return res.json({
                  ...responses.login,
                  data: {
                    user: userObj,
                    weekoffs,
                    holidays,
                    notifications
                  },
                  authToken
                });
              }
            } else return res.json(responses.notExist);
          });
        }
      } catch (error) {
        console.log(`${errors.tryCatch}login`, err);
        return res.json(responses.error);
      }
    }
  );
};

/**
 * @method changePassword: This method use to change the user password
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.changePassword = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "id", name: "Employee Data ID" },
      { key: "old", name: "Old Password" },
      { key: "new", name: "New Password" },
      { key: "confirm", name: "Confirm Password" }
    ],
    res,
    error => {
      try {
        if (!error) {
          const { id, old } = req.body;
          const query = User.findOne({
            $and: [{ _id: ObjectID(id) }, { isDeleted: false }]
          }).lean();
          query.exec(async (err, user) => {
            if (err) {
              console.log(`${errors.query}changePassword`, err);
              return res.json(responses.error);
            }
            if (user) {
              if (req.body.new !== req.body.confirm) return res.json(responses.notMatchPwd);
              if (!user.password) return res.json(responses.notPassword);
              const isOldPwdRight = await pwdCont.compare(old, user.password);
              if (isOldPwdRight === false) return res.json(responses.wrongOldPwd);
              if (old === req.body.new) return res.json(responses.diffPwd);
              else {
                // Function to get hashed password
                const hash = await pwdCont.generate(req.body.new);
                await User.updateOne({ _id: ObjectID(id) }, {
                  $set: { password: hash }
                });
                return res.json(responses.changePwdSuccess);
              }
            } else return res.json(responses.notExist);
          });
        }
      } catch (error) {
        console.log(`${errors.tryCatch}changePassword`, err);
        return res.json(responses.error);
      }
    }
  );
};

/**
 * @method forgotPassword: This method use to send set new password link in your account email
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.forgotPassword = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "email", name: "Email" }
    ],
    res,
    error => {
      try {
        if (!error) {
          const { email } = req.body;
          const cond = { $and: [{ email }, { isDeleted: false }] };
          const query = User.findOne(cond).lean();
          query.exec(async (err, user) => {
            if (err) {
              console.log(`${errors.query}forgotPassword`, err);
              return res.json(responses.error);
            }
            if (user) {
              const token = middleware.getToken(email, "forgotPassword");
              await User.updateOne(cond, { $set: { token } });
              const url = `${credentials.frontendURL}/forgot-password?t=${token}`;
              const content = mailFormat.fgtPwdContent({ url, user });
              const mailObj = {
                to: { name: user.name, email: user.email },
                from: { name: Innow8.name, email: Innow8.email },
                subject: content.subject,
                html: content.body
              }
              // send email
              mailCtr.Mailer(mailObj);
              return res.json(responses.mailSuccess);
            } else return res.json(responses.notExist);
          })
        }
      } catch (err) {
        console.log(`${errors.tryCatch} forgotPassword`, err);
        return res.json(responses.error);
      }
    }
  );
};

/**
 * @method verifyToken: This method use to set new user password
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.verifyToken = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "token", name: "Token" },
      { key: "password", name: "Password" },
      { key: "confirm", name: "Confirm password" }
    ],
    res,
    error => {
      try {
        if (!error) {
          const { password, token } = req.body;
          const cond = { token };
          const query = User.findOne(cond).lean();
          query.exec(async (err, user) => {
            if (err) {
              console.log(`${errors.query}verifyToken`, err);
              return res.json(responses.error);
            }
            if (user) {
              if (password !== req.body.confirm) {
                return res.json(responses.notMatchPwd);
              } else if (!(await middleware.verifyToken(token))) {
                return res.json(responses.expireToken);
              } else {
                // Function to get hashed password
                const hash = await pwdCont.generate(password);
                await User.updateOne(cond, { $set: { password: hash } });
                return res.json(responses.updateSuccess("Password"));
              }
            } else return res.json(responses.wrongInput("Link"));
          })
        }
      } catch (error) {
        console.log(`${errors.tryCatch}verifyToken`, err);
        return res.json(responses.error);
      }
    }
  );
};

module.exports.getHolidays = getHolidays;
