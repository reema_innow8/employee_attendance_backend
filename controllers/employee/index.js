"use strict";
const User = require("../../models/user");
const Salary = require("../../models/salary");
const Leave = require("../../models/leave");
const util = require("../../common/utils");
const mongoose = require("mongoose");
const ObjectID = mongoose.Types.ObjectId;
const pwdCont = require("../../common/password");
const responses = require("../../common/responses");
const Attendance = require("../../models/attendance");
const errors = require("../../common/errors");
const path = require('path');
const moment = require('moment-timezone');
const lineReader = require('line-reader');
const mailCtr = require("../../common/mailer");
const notificationCtr = require("../notification/index");
const credentials = require('../../credentials');
const Innow8 = credentials.Innow8;
const mailFormat = require('../../common/mailFormat');
const employeeIDFormat = require('../../common/employeeIDFormat');

/**
 * @method add: This method use to add new company employee
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.add = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "empId", name: "Employee company ID" },
      { key: "userId", name: "Admin ID" },
      { key: "name", name: "Name" },
      { key: "email", name: "Email" },
      { key: "contact", name: "Contact" },
      { key: "timeZone", name: "Time Zone" },
      { key: "joiningDate", name: "Joining date" }
    ],
    res,
    error => {
      try {
        if (!error) {
          let { contact, empId } = req.body;
          contact = String(contact.trim());
          empId = String(empId.trim());
          const isValid = employeeIDFormat.checkEmpID(empId);
          if (isValid === false) return res.json(responses.invalidInput('employee ID'));
          const lowCaseEmail = req.body.email.trim().toLowerCase();
          const query = User.findOne({
            $and: [
              { $or: [{ email: lowCaseEmail }, { contact }, { empId }] },
              { isDeleted: false }
            ]
          }).lean();
          query.exec(async (err, employee) => {
            if (err) {
              console.log(`${errors.query}add`, err);
              return res.json(responses.error);
            }
            if (employee) {
              if (employee.contact === contact) return res.json(responses.contactAlready);
              else if (employee.email === lowCaseEmail) return res.json(responses.emailAlready);
              else return res.json(responses.idAlready);
            } else {
              const password = pwdCont.pwdCreator();
              const hash = await pwdCont.generate(password);

              let emp = new User({
                empId,
                admin: ObjectID(req.body.userId),
                name: req.body.name,
                email: lowCaseEmail,
                contact,
                type: "employee",
                password: hash,
                timeZone: req.body.timeZone,
                joiningDate: req.body.joiningDate
              });
              // Store hashed password and other details of employee in database
              emp.save().then(async result => {
                const content = mailFormat.pwdContent({ ...JSON.parse(JSON.stringify(result)), password });
                const mailObj = {
                  to: { name: result.name, email: result.email },
                  from: { name: Innow8.name, email: Innow8.email },
                  subject: content.subject,
                  html: content.body
                }
                // send email
                mailCtr.Mailer(mailObj);
                return res.json(responses.empSuccess('add'));
              }).catch(err => {
                console.log(`${errors.query}add`, err);
                return res.json({ ...responses.error, error: err });
              });
            }
          });
        }
      } catch (error) {
        console.log(`${errors.tryCatch}add`, error);
        return res.json(responses.error);
      }
    }
  );
};

/**
 * @method orgEmployees: This method use to fetch company all employees
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.fetch = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "id", name: "Data ID" },
      { key: "limit", name: "Limit" },
      { key: "offset", name: "Offset" }
    ],
    res,
    async error => {
      try {
        if (!error) {
          const { search, limit, offset } = req.body;
          const id = ObjectID(req.body.id);
          const text = search && search.trim() || "";
          const cond = [{ isDeleted: false }];
          if (text) {
            cond.push({ $or: [{ name: { $regex: text, $options: "i" } }, { empId: { $regex: text, $options: "i" } }] });
          }
          const user = await User.findOne({ _id: id }).lean();
          if (!user) return res.json(responses.wrongInput("ID"));
          if (user.type && user.type !== "admin") {
            cond.push({ _id: ObjectID(user._id) }, { type: "employee" });
          } else cond.push({ admin: id });

          const query = User.find({ $and: cond })
            .select({
              empId: 1,
              name: 1,
              email: 1,
              contact: 1,
              joiningDate: 1
            })
            // .limit(limit)
            // .skip(offset)
            .lean();
          query.exec(async (err, employees) => {
            if (err) {
              console.log(`${errors.query}fetch`, err);
              return res.json(responses.error);
            }
            if (employees && employees.length > 0) {
              // const count = await User.find({ $and: cond }).countDocuments();
              const data = {
                employees: employees.slice(offset, offset + limit), count: employees.length
              }
              return res.json({
                ...responses.fetchSuccess, data
              });
            } else return res.json(responses.noRecord("Employee"));
          });
        }
      } catch (error) {
        console.log(`${errors.tryCatch}fetch`, error);
        return res.json(responses.error);
      }
    }
  );
};

/**
 * @method remove: This method use to delete existing employee account from company
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.remove = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [{ key: "id", name: "Employee data ID" }],
    res,
    error => {
      try {
        if (!error) {
          const { id } = req.body;
          const query = User.findOne({ _id: ObjectID(id) }).lean();
          query.exec(async (err, employee) => {
            if (err) {
              console.log(`${errors.query}remove`, err);
              return res.json(responses.error);
            }
            if (employee) {
              if (employee.isDeleted === true) return res.json(responses.deletedAlready);
              else {
                const isDeleted = { $set: { isDeleted: true } };
                const empCond = { employee: ObjectID(id) };
                await User.updateOne({ _id: ObjectID(id) }, isDeleted);
                await Salary.updateOne(empCond, isDeleted);
                await Leave.updateMany(empCond, isDeleted);
                await Attendance.updateMany(empCond, isDeleted);
                return res.json(responses.empSuccess("deleted"));
              }
            } else return res.json(responses.wrongInput("ID"));
          });
        }
      }
      catch (error) {
        console.log(`${errors.tryCatch}remove`, error);
        return res.json(responses.error);
      }
    }
  );
};

/**
 * @method edit: This method use to update the employee detail
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.edit = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "id", name: "Employee data ID" },
      { key: "empId", name: "Employee company ID" },
      { key: "name", name: "Name" },
      { key: "email", name: "Email" },
      { key: "contact", name: "Contact" },
      { key: "joiningDate", name: "Joining date" }
    ],
    res,
    error => {
      try {
        if (!error) {
          const { id, joiningDate } = req.body;
          const ID = ObjectID(id);
          let { contact, empId } = req.body;
          contact = String(contact.trim());
          empId = String(empId.trim());
          const isValid = employeeIDFormat.checkEmpID(empId);
          if (isValid === false) return res.json(responses.invalidInput('employee ID'));
          const lowCaseEmail = req.body.email.trim().toLowerCase();
          const query = User.findOne({
            $and: [
              { $or: [{ email: lowCaseEmail }, { contact }, { empId }] },
              { _id: { $ne: ID } },
              { isDeleted: false }
            ]
          }).lean();
          query.exec(async (err, employee) => {
            if (err) {
              console.log(`${errors.query}edit`, err);
              return res.json(responses.error);
            }
            if (employee) {
              if (employee.contact === contact) return res.json(responses.contactAlready);
              else if (employee.email === lowCaseEmail) return res.json(responses.emailAlready);
              else return res.json(responses.idAlready);
            } else {
              const emp = {
                empId,
                name: req.body.name,
                email: lowCaseEmail,
                contact,
                joiningDate
              };
              await User.updateOne({ _id: ID }, { $set: emp });
              await notificationCtr.create({
                user: ID, message: 'Admin has updated your profile.', type: "user"
              });
              return res.json(responses.empSuccess("updated"));
            }
          });
        }
      }
      catch (error) {
        console.log(`${errors.tryCatch}edit`, error);
        return res.json(responses.error);
      }
    }
  );
};

/**
 * @method storeAttendance: This method use to store employee attendance entries
 * @param {String} line attendance entry of employee from previous/parent method
 * @param {String} timeZone admin time-zone from previous/parent method
 * @param {Array} idList employees id list from previous/parent method
 */
const storeAttendance = async (line, timeZone, idList) => {
  if (line.includes("No") === true) return;
  const temp = line.split("");
  temp.push(" ");
  let entry = [];
  let str = "";
  for (let i = 0; i < temp.length; i++) {
    let t = temp[i];
    if (t === '\t' || t === " ") {
      entry.push(str);
      str = "";
    } else {
      str += t;
    }
    let j = i + 1;
    let k = 0;
    for (; temp[j] === '\t' || temp[j] === " "; j++, k++) { }
    if (k > 0) i += k - 1;
  }
  if (entry.length === 8) {
    const index = idList.findIndex(i => parseInt(entry[2]) === i.empId);
    if (index > -1) {
      let mode = "";
      switch (entry[4]) {
        case "1":
          mode = "entry";
          break;
        case "10":
          mode = "exit";
      }
      if (mode) {
        const date = entry[6].split(".");
        const time = entry[7].split(":");
        const dateStr = `${date[2]}-${date[1]}-${date[0]} ${time[0]}:${time[1]}:${time[2]}`;
        const dateTime = moment.tz(dateStr, timeZone).format();
        const obj = {
          employee: ObjectID(idList[index].id),
          [mode]: dateTime,
          createdAt: dateTime
        }
        await new Attendance(obj).save();
      }
    }
  }
}

/**
 * @method attendance: This method use to store all employees attendace records in our database
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.attendance = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [{ key: "id", name: "Admin ID" }],
    res,
    async error => {
      try {
        if (!error) {
          const { file } = req;
          const { id } = req.body;
          if (!file || Object.keys(file).length === 0) {
            return res.json(responses.missingFile);
          }
          const ID = ObjectID(id);
          const fileName = "attendance.txt";
          const filePath = path.join(__dirname, `../../public/${fileName}`);
          let timeZone = "";
          const users = await User.find({ $and: [{ $or: [{ _id: ID }, { admin: ID }] }, { isDeleted: false }] }).lean();
          const idList = [];
          for (let user of users) {
            if (String(user._id) === String(id)) {
              timeZone = user.timeZone;
            } else if (user.empId) {
              const empId = parseInt(user.empId.replace(/\D/g, ''));
              idList.push({ id: user._id, empId });
            }
          }
          if (timeZone) {
            if (idList.length > 0) {
              lineReader.eachLine(filePath, line => {
                storeAttendance(line, timeZone, idList);
              });
            }
            return res.json(responses.attendanceSuccess);
          } else {
            return res.json(responses.wrongInput("ID"));
          }
        }
      } catch (error) {
        console.log(`${errors.tryCatch}attendance`, error);
        return res.json(responses.error);
      }
    }
  );
};

/**
 * @method uploadImage: This method use to upload image
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.uploadImage = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "id", name: "ID" },
      { key: "url", name: "Image URL" }
    ],
    res,
    async error => {
      try {
        if (!error) {
          const ID = ObjectID(req.body.id);
          const query = User.find({ _id: ID }).lean();
          query.exec(async (err, user) => {
            if (err) {
              console.log(`${errors.query}uploadImage`, err);
              return res.json(responses.error);
            }
            if (user) {
              await User.updateOne({ _id: ID }, { $set: { image: req.body.url } });
              return res.json(responses.uploadSuccess('Image'));
            } else {
              return res.json(responses.wrongInput('ID'));
            }
          });
        }
      } catch (error) {
        console.log(`${errors.tryCatch}uploadImage`, error);
        return res.json(responses.error);
      }
    }
  );
};
