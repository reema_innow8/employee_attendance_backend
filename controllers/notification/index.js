const Notification = require('../../models/notification');
const User = require('../../models/user');
const errors = require("../../common/errors");
const mongoose = require('mongoose');
const ObjectID = mongoose.Types.ObjectId;
const moment = require('moment-timezone');
const responses = require("../../common/responses");
const util = require("../../common/utils");
const credentials = require('../../credentials');

/**
 * @method create: This method use to create new notification
 * @param {Object} obj data from previous/parent api method
 */
module.exports.create = async obj => {
    let notification;
    try {
        notification = await new Notification(obj).save();
        return notification;
    } catch (error) {
        console.log(`${errors.tryCatch}create`, error);
        return notification;
    }
}

/**
 * @method fetch: This method use to fetch last month notifications
 * @param {Object} obj time-zone and user id from previous/parent api method
 */
const fetch = async obj => {
    let notifications = [];
    try {
        const { timeZone, _id } = obj;
        if (!timeZone || !_id) return notifications;
        const end = (timeZone ? moment().tz(timeZone) : moment()).format();
        const start = moment(end).subtract(credentials.notificationDaysLimit, 'days').format();
        notifications = await Notification.find({
            $and: [
                { user: ObjectID(_id) },
                { createdAt: { $gte: new Date(start), $lte: new Date(end) } }
            ]
        }).sort({ createdAt: -1 }).populate({ path: 'leave', populate: { path: "employee" } }).lean();
        return notifications;
    } catch (error) {
        console.log(`${errors.tryCatch}fetch`, error);
        return notifications;
    }
}

/**
 * @method getNotifications: This method use to fetch last month notifications
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.getNotifications = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: 'id', name: 'User ID' }
        ],
        res,
        error => {
            try {
                if (!error) {
                    const query = User.findOne({ _id: ObjectID(req.body.id) }).select({ timeZone: 1 }).lean();
                    query.exec(async (err, user) => {
                        if (err) {
                            console.log(`${errors.query}getNotifications`, err);
                            return res.json(responses.error);
                        }
                        if (user) {
                            const notifications = await fetch(user);
                            return res.json({ ...responses.fetchSuccess, data: notifications });
                        } else return res.json(responses.wrongInput("ID"));
                    });
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}getNotifications`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method read: This method use to read notifications
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.read = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: 'id', name: "ID's list" }
        ],
        res,
        error => {
            try {
                if (!error) {
                    const { id } = req.body;
                    const list = [];
                    if (Array.isArray(id)) {
                        for (let item of id) {
                            list.push(ObjectID(item));
                        }
                    } else if (typeof id === "string") {
                        list.push(ObjectID(id));
                    } else {
                        return res.json(responses.wrongInput("ID"));
                    }
                    const query = Notification.updateMany({ _id: { $in: list } }, { $set: { isRead: true } });
                    query.exec(async (err, isUpdate) => {
                        if (err) {
                            console.log(`${errors.query}read`, err);
                            return res.json(responses.error);
                        }
                        if (isUpdate) {
                            return res.json(responses.updateSuccess('Notifications'));
                        } else return res.json(responses.wrongInput("ID"));
                    });
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}read`, error);
                return res.json(responses.error);
            }
        }
    );
};


module.exports.fetch = fetch;