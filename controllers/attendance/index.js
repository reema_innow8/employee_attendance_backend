"use strict";
const Attendance = require("../../models/attendance");
const User = require("../../models/user");
const util = require("../../common/utils");
const responses = require("../../common/responses"); 0
const mongoose = require("mongoose");
const ObjectID = mongoose.Types.ObjectId;
const errors = require("../../common/errors");
const authCtr = require("../authentication/index");
const salaryCtr = require("../salary/index");
const moment = require('moment-timezone');

/**
 * @method fetch: This method use to fetch attendance records for any specific employee
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.fetch = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "id", name: "Employee data ID" },
      { key: "from", name: "Start Date" },
      { key: "to", name: "End Date" }
    ],
    res,
    error => {
      try {
        if (!error) {
          const query = Attendance.aggregate([
            {
              $match: { $and: [{ employee: ObjectID(req.body.id) }, { isDeleted: false }, { createdAt: { $gte: new Date(req.body.from), $lte: new Date(req.body.to) } }] }
            },
            {
              $group: {
                _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
                entry: {
                  $addToSet: "$$ROOT.entry"
                },
                exit: {
                  $addToSet: "$$ROOT.exit"
                }
              }
            },
            { $sort: { _id: -1 } }
          ])
          query.exec((err, entries) => {
            if (err) {
              console.log(`${errors.query}fetch`, err);
              return res.json(responses.error);
            }
            return res.json({ ...responses.fetchSuccess, data: entries });
          });
        }
      } catch (error) {
        console.log(`${errors.tryCatch}fetch`, error);
        return res.json(responses.error);
      }
    }
  );
};

/**
 * @method fetchAll: This method use to fetch attendance records for all company employees
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.fetchAll = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "id", name: "Admin ID" },
      { key: "from", name: "Start Date" },
      { key: "to", name: "End Date" }
    ],
    res,
    async error => {
      try {
        if (!error) {
          const { to, from } = req.body;
          const id = ObjectID(req.body.id);
          const dateCond = { $gte: new Date(from), $lte: new Date(to) };
          const cond = [{ isDeleted: false }, { createdAt: dateCond }];
          const user = await User.findOne({ _id: id }).lean();
          if (!user) return res.json(responses.wrongInput("ID"));
          if (user.type && user.type !== "admin") cond.push({ employee: ObjectID(user._id) });
          Promise.all([
            Attendance.aggregate([
              {
                $match: { $and: cond }
              },
              {
                $group:
                {
                  _id: {
                    _id: "$employee",
                    date: { $dateToString: { format: "%d-%m-%Y", date: "$createdAt" } },
                  },
                  entry: {
                    $addToSet: "$$ROOT.entry"
                  },
                  exit: {
                    $addToSet: "$$ROOT.exit"
                  }
                }
              },
              {
                $sort: { _id: 1 }
              },
              {
                $group:
                {
                  _id: "$_id._id",
                  list:
                  {
                    $push:
                    {
                      date: "$_id.date",
                      entry: "$entry",
                      exit: "$exit",
                    }
                  }
                }
              },
              {
                $lookup:
                {
                  from: "users",
                  localField: "_id",
                  foreignField: "_id",
                  as: "employee"
                }
              },
              {
                $lookup:
                {
                  from: "leaves",
                  as: "leaves",
                  let: { employee: "$_id" },
                  pipeline: [
                    {
                      $match:
                      {
                        $expr:
                        {
                          $and:
                            [
                              { $eq: ["$$employee", "$employee"] },
                              { $eq: ["$status", "approved"] },
                            ]
                        }
                      }
                    },
                    { $match: { dates: { $elemMatch: dateCond } } }
                  ],
                }
              }
            ]),
            User.find({ $and: [{ isDeleted: false }, { admin: id }] }).lean(),
            authCtr.getHolidays({ from, to })
          ]).then(result => {
            return res.json({
              ...responses.fetchSuccess,
              data: {
                attendance: result[0],
                employees: result[1],
                holidays: result[2]
              }
            });
          }).catch(err => {
            console.log(`${errors.query}fetchAll`, err);
            return res.json(responses.error);
          })
        }
      } catch (error) {
        console.log(`${errors.tryCatch}fetchAll`, error);
        return res.json(responses.error);
      }
    }
  );
};

const getEntries = obj => {
  const { status, holidays, weekoffs, date } = obj;
  const isHoliday = salaryCtr.checkHoliday({ holidays, weekoffs, dateObj: { date, onlyDate: date.format("DD-MM-YYYY") } });
  if (isHoliday === true) {
    switch (status) {
      case "present":
        return [
          { mode: "entry", time: "09:00:00" },
          { mode: "exit", time: "18:00:00" }
        ];
      case "half_day":
        return [
          { mode: "entry", time: "09:00:00" },
          { mode: "exit", time: "15:00:00" }
        ];
      case "short_leave":
        return [
          { mode: "entry", time: "09:00:00" },
          { mode: "exit", time: "17:00:00" }
        ];
    }
  } else {
    switch (status) {
      case "present":
        return [
          { mode: "entry", time: "09:00:00" },
          { mode: "exit", time: "17:00:00" }
        ];
      case "half_day":
        return [
          { mode: "entry", time: "09:00:00" },
          { mode: "exit", time: "13:00:00" }
        ];
      case "short_leave":
        return [
          { mode: "entry", time: "09:00:00" },
          { mode: "exit", time: "15:00:00" }
        ];
    }
  }
}

/**
 * @method edit: This method use to fetch attendance records for all company employees
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.edit = (req, res) => {
  util.validateRequiredKeys(
    req.body,
    [
      { key: "id", name: "Admin ID" },
      { key: "empId", name: "Employee ID" },
      { key: "dates", name: "Dates" },
      { key: "status", name: "Status" }
    ],
    res,
    async error => {
      try {
        if (!error) {
          const { dates } = req.body;
          const id = ObjectID(req.body.id);
          const status = req.body.status.trim().toLowerCase();
          if (status !== 'absent' && status !== 'present' && status !== 'half_day' && status !== 'short_leave') {
            return res.json(responses.wrongInput("Status"));
          }
          const users = await User.find({ $or: [{ _id: id }, { empId: req.body.empId }] }).select({ type: 1, timeZone: 1 }).lean();
          let admin = "", employee = "";
          users.forEach(i => {
            if (i.type === "admin") {
              admin = i;
            } else {
              employee = i
            }
          })
          if (!admin || !employee) return res.json(responses.wrongInput("ID"));
          const { timeZone } = admin;
          for (let date of dates) {
            const dateTime = timeZone ? moment(date).tz(timeZone) : moment(date);
            const start = dateTime.startOf('day').format();
            const end = dateTime.endOf('day').format();
            const dateCond = { $gte: new Date(start), $lte: new Date(end) };
            await Attendance.deleteMany({ $and: [{ employee: ObjectID(employee._id) }, { createdAt: dateCond }] });
          }
          const { holidays, weekoffs } = await salaryCtr.holidayWeekend({ timeZone, dateCond: { $in: dates } });
          if (status !== "absent") {
            const dataList = [];
            for (let date of dates) {
              const momentDate = timeZone ? moment(date).tz(timeZone) : moment(date);
              const entries = getEntries({ status, holidays, weekoffs, date: momentDate });
              for (let entry of entries) {
                const dateStr = `${momentDate.format("YYYY-MM-DD")} ${entry.time}`;
                const dateTime = (timeZone ? moment(dateStr).tz(timeZone) : moment(dateStr)).format();
                const obj = {
                  employee: ObjectID(employee._id),
                  [entry.mode]: dateTime,
                  createdAt: dateTime
                }
                dataList.push(obj);
              }
            }
            await Attendance.insertMany(dataList);
          }
          return res.json(responses.attendanceSuccess);
        }
      } catch (error) {
        console.log(`${errors.tryCatch}edit`, error);
        return res.json(responses.error);
      }
    }
  );
};
