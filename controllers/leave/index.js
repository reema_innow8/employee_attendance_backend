"use strict";
const util = require("../../common/utils");
const responses = require("../../common/responses");
const errors = require("../../common/errors");
const Leave = require('../../models/leave');
const User = require('../../models/user');
const mongoose = require('mongoose');
const ObjectID = mongoose.Types.ObjectId;
const mailCtr = require("../../common/mailer");
const notificationCtr = require("../../controllers/notification");
const salaryCtr = require("../../controllers/salary");
const credentials = require("../../credentials");
const Innow8 = credentials.Innow8;
const moment = require('moment-timezone');
const mailFormat = require('../../common/mailFormat');

/**
 * @method addHolidays: This method use to add dates between leave dates
 * @param {Object} obj dates, timeZone, weekoffs and holidays from previous/parent api method
 */
const addHolidays = obj => {
    let arr = [];
    try {
        const { dates, timeZone, weekoffs, holidays } = obj;
        dates.sort((a, b) => new Date(a) - new Date(b));
        arr = [...dates];
        const endDate = new Date(dates[dates.length - 1]);
        for (let i = 0; new Date(dates[i]) <= endDate; i++) {
            const days = salaryCtr.dateDifference(dates[i], dates[i + 1]).days;
            if (days > 1) {
                let startDate, endDate;
                if (timeZone) {
                    startDate = moment(dates[i]).tz(timeZone).add(1, "days");
                    endDate = moment(dates[i + 1]).tz(timeZone).subtract(1, "days");
                } else {
                    startDate = moment(dates[i]).add(1, "days");
                    endDate = moment(dates[i + 1]).subtract(1, "days");
                }
                const dateArr = salaryCtr.getDates({ start: startDate.format(), end: endDate.format() }, timeZone);
                const index =
                    dateArr &&
                    dateArr.findIndex((i) => {
                        const onlyDate = (timeZone ? moment(i).tz(timeZone) : moment(i)).format("DD-MM-YYYY");
                        const obj = { weekoffs, holidays, dateObj: { date: i, onlyDate } };
                        if (salaryCtr.checkHoliday(obj) === false) return true;
                    });
                if (index < 0) arr.push(...dateArr);
            }
        }
        arr.sort((a, b) => new Date(a) - new Date(b));
        return arr;
    } catch (error) {
        console.log(`${errors.tryCatch}addHolidays`, error);
        return arr;
    }
};

/**
 * @method apply: This method use to short leave dates and return string
 * @param {Object} obj timeZone and dates from previous/parent api method
 */
const shortDates = obj => {
    let str = "";
    try {
        const { timeZone } = obj;
        let { dates } = obj;
        if (dates && dates.length > 0) {
            dates = addHolidays(obj);
            for (let i = 0; i < dates.length; i++) {
                let prev = i;
                let j = i;
                let k = 0;
                for (
                    ;
                    dates[j] &&
                    dates[j + 1] &&
                    salaryCtr.dateDifference(dates[j], dates[j + 1]).days === 1;
                    k++, j++
                ) { }
                if (k > 0) {
                    str += (timeZone ? moment(dates[prev]).tz(timeZone) : moment(dates[prev])).format("DD/MM/YYYY");
                    if (k > 1) str += " to ";
                    else str += ", ";
                    i += k;
                }
                str += (timeZone ? moment(dates[i]).tz(timeZone) : moment(dates[i])).format("DD/MM/YYYY");
                if (i < dates.length - 1) str += ", ";
            }
        }
        return str;
    } catch (error) {
        console.log(`${errors.tryCatch}shortDates`, error);
        return str;
    }
};

/**
 * @method apply: This method use to send leave for any specific employee
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.apply = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: "userId", name: "User ID" },
            { key: "id", name: "Employee company ID" },
            { key: "subject", name: "Subject" },
            { key: "message", name: "Content" },
            { key: "dates", name: "Dates" },
            { key: "type", name: "Type" }
        ],
        res,
        error => {
            try {
                if (!error) {
                    const { subject, message, id, dates, userId } = req.body;
                    let { type } = req.body;
                    type = type.toLowerCase().trim();
                    const query = User.find({
                        $and:
                            [
                                {
                                    $or: [
                                        { $and: [{ empId: id }, { type: "employee" }] },
                                        { _id: ObjectID(userId) }
                                    ]
                                },
                                { isDeleted: false }
                            ]
                    }).lean();
                    query.exec((err, users) => {
                        if (err) {
                            console.log(`${errors.query}apply`, err);
                            return res.json(responses.error);
                        }
                        if (users && users.length > 0) {
                            let emp, admin;
                            users.forEach(i => {
                                if (i.type === "admin") admin = i;
                                else emp = i;
                            })
                            let leaveType = "";
                            if (type.includes("full")) {
                                leaveType = "full_day";
                            } else if (type.includes("half")) {
                                leaveType = "half_day";
                            } else if (type.includes("short")) {
                                leaveType = "short_leave";
                            } else {
                                return res.json(responses.invalidInput("leave type"));
                            }
                            const obj = {
                                employee: emp._id,
                                subject,
                                message,
                                status: "sent",
                                dates,
                                type: leaveType
                            }
                            new Leave(obj).save().then(async result => {
                                if (result) {
                                    const { ceo, cto, hr } = Innow8;
                                    const mailObj = {
                                        to: { name: ceo.name, email: ceo.email },
                                        cc: [
                                            { name: cto.name, email: cto.email },
                                            { name: hr.name, email: hr.email }
                                        ],
                                        from: { name: emp.name, email: emp.email },
                                        subject,
                                        html: `<p>${message}</p>`
                                    }
                                    // send email
                                    // mailCtr.Mailer(mailObj);
                                    let obj;
                                    const { timeZone } = emp;
                                    const { holidays, weekoffs } = await salaryCtr.holidayWeekend({ timeZone, dateCond: { $in: dates } });
                                    const leaveDates = shortDates({ holidays, weekoffs, dates, timeZone });
                                    if (admin) {
                                        obj = {
                                            user: ObjectID(emp._id),
                                            message: `${salaryCtr.titleCase(admin.name)} applied leave for ${leaveDates}.`
                                        }
                                    } else {
                                        obj = {
                                            user: ObjectID(emp.admin),
                                            message: `${salaryCtr.titleCase(emp.name)}(${emp.empId}) applied leave for ${leaveDates}.`
                                        }
                                    }
                                    await notificationCtr.create({ ...obj, type: "leave", leave: ObjectID(result._id) });
                                    return res.json(responses.leaveSuccess("apply"));
                                }
                                else return res.json(responses.error);
                            });
                        } else return res.json(responses.wrongInput("ID"));
                    })
                }
            } catch (error) {
                console.log(`${errors.tryCatch}apply`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method fetch: This method use to fetch all employee leaves
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.fetch = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: "id", name: "Data ID" },
            { key: "limit", name: "Limit" },
            { key: "offset", name: "Offset" }
        ],
        res,
        async error => {
            try {
                if (!error) {
                    const { search, limit, offset } = req.body;
                    const cond = [{ isDeleted: false }];
                    const user = await User.findOne({ _id: ObjectID(req.body.id) });
                    if (!user) return res.json(responses.wrongInput("ID"));
                    if (user.type && user.type !== "admin") cond.push({ employee: ObjectID(user._id) });
                    const empCond = { path: "employee", select: { _id: 0, empId: 1, name: 1 } };
                    const text = search && search.trim() || "";
                    if (text) {
                        empCond["match"] = { $or: [{ name: { $regex: text, $options: "i" } }, { empId: { $regex: text, $options: "i" } }] };
                    }
                    const query = Leave.find({ $and: cond })
                        // .limit(limit)
                        // .skip(offset)
                        .populate(empCond)
                        .sort({ updatedAt: -1 }).lean();
                    query.exec(async (err, leaves) => {
                        if (err) {
                            console.log(`${errors.query}fetch`, err);
                            return res.json(responses.error);
                        }
                        if (leaves && leaves.length > 0) {
                            let filtered = leaves.filter(i => i.employee);
                            const data = {
                                leaves: filtered.slice(offset, offset + limit), count: filtered.length
                            }
                            return res.json({ ...responses.fetchSuccess, data });
                        }
                        else return res.json(responses.noRecord("Leave"));
                    });
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}fetch`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method reply: This method use to send employee leave reponse through email
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.reply = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: 'userId', name: 'User ID' },
            { key: 'id', name: 'Leave ID' },
            { key: 'status', name: 'Status' }
        ],
        res,
        error => {
            try {
                if (!error) {
                    let { status, reason, id, userId } = req.body;
                    status = status.trim().toLowerCase();
                    if ((status !== 'approved') && (status !== 'rejected') && (status !== 'cancelled')) {
                        return res.json(responses.wrongInput("Status"));
                    }
                    const cond = { _id: ObjectID(id) };
                    const query = Leave.findOne(cond)
                        .select({ employee: 1, subject: 1, type: 1, status: 1, dates: 1 })
                        .populate({ path: 'employee', select: { name: 1, email: 1, empId: 1 } }).lean();
                    query.exec(async (err, leave) => {
                        if (err) {
                            console.log(`${errors.query}reply`, err);
                            return res.json(responses.error);
                        }
                        if (leave) {
                            const { dates, _id } = leave;
                            let obj = { status, reason: reason || "" };
                            if (status === "cancelled") {
                                obj.isCancel = false;
                            }
                            await Leave.updateOne(cond, { $set: obj });
                            const oldStatus = leave.status;
                            if (oldStatus !== status) {
                                const { employee } = leave;
                                // let leaves = 0.0;
                                // switch (leave.type) {
                                //     case "full_day":
                                //         leaves = 1.0;
                                //         break;
                                //     case "half_day":
                                //         leaves = 0.5;
                                //         break;
                                //     case "short_leave":
                                //         leaves = 0.25;
                                // }
                                // if (status === "rejected" || status === "cancelled") {
                                //     if (oldStatus === "approved") leaves = -leaves;
                                //     else if (oldStatus === "sent") leaves = 0.0;
                                // }
                                // const empCond = { _id: ObjectID(employee._id) };
                                // const user = await User.findOne(empCond);
                                // if (leaves !== 0.0) {
                                //     for (let item of leave.dates) {
                                //         const date = moment(item).tz(user.timeZone).format('YYYY-MM');
                                //         const isExist = user.leaves.some(i => i.date === date);
                                //         const data = {};
                                //         if (isExist === true) {
                                //             empCond["leaves.date"] = date;
                                //             data['$inc'] = { "leaves.$.count": leaves };
                                //         } else {
                                //             data['$push'] = { leaves: { count: leaves, date } };
                                //         }
                                //         await User.updateOne(empCond, data);
                                //     }
                                // }
                                const content = mailFormat.leaveReplyContent({ subject: leave.subject, status, reason });
                                const mailObj = {
                                    to: { name: employee.name, email: employee.email },
                                    from: { name: Innow8.name, email: Innow8.email },
                                    subject: content.subject,
                                    html: content.body
                                }
                                // send email
                                mailCtr.Mailer(mailObj);
                                const admin = await User.findOne({
                                    $and: [
                                        { _id: ObjectID(userId) },
                                        { type: 'admin' },
                                        { isDeleted: false }
                                    ]
                                }).lean();
                                let obj = {};
                                const { timeZone } = admin;
                                const { holidays, weekoffs } = await salaryCtr.holidayWeekend({ timeZone, dateCond: { $in: dates } });
                                const leaveDates = shortDates({ holidays, weekoffs, dates, timeZone });
                                if (admin) {
                                    obj = {
                                        user: ObjectID(employee._id),
                                        message: `${salaryCtr.titleCase(admin.name)} has ${status} your leave for ${leaveDates}.`
                                    }
                                } else {
                                    obj = {
                                        user: ObjectID(employee.admin),
                                        message: `${salaryCtr.titleCase(employee.name)}(${employee.empId}) has ${status} leave for ${leaveDates}.`
                                    }
                                }
                                await notificationCtr.create({ ...obj, type: "leave", leave: ObjectID(_id) });
                                return res.json(responses.leaveSuccess(status));
                            }
                            return res.json(responses.updateSuccess("Leave"));
                        }
                        else return res.json(responses.wrongInput("ID"));
                    });
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}reply`, error);
                return res.json(responses.error);
            }
        }
    );
};

/**
 * @method cancellationRequest: This method use to send leave cancellation request to admin
 * @param {Object} req api request with data from user
 * @param {Object} res response object to user
 */
module.exports.cancellationRequest = (req, res) => {
    util.validateRequiredKeys(
        req.body,
        [
            { key: 'id', name: 'Leave ID' }
        ],
        res,
        error => {
            try {
                if (!error) {
                    const cond = { _id: ObjectID(req.body.id) };
                    const query = Leave.findOne(cond).populate('employee').lean();
                    query.exec(async (err, leave) => {
                        if (err) {
                            console.log(`${errors.query}cancellationRequest`, err);
                            return res.json(responses.error);
                        }
                        if (leave) {
                            const { dates, employee, _id } = leave;
                            if (leave.isCancel === true) return res.json(responses.leaveAlready);
                            await Leave.updateOne(cond,
                                { $set: { isCancel: true } }
                            );
                            const { timeZone } = employee;
                            const { holidays, weekoffs } = await salaryCtr.holidayWeekend({ timeZone, dateCond: { $in: dates } });
                            const leaveDates = shortDates({ holidays, weekoffs, dates, timeZone });
                            await notificationCtr.create({
                                user: ObjectID(employee.admin),
                                message: `${salaryCtr.titleCase(employee.name)}(${employee.empId}) has requested leave cancellation for ${leaveDates}.`,
                                type: "leave",
                                leave: ObjectID(_id)
                            });
                            return res.json(responses.leaveSuccess("cancellation request sent"));
                        } else {
                            return res.json(responses.wrongInput("ID"));
                        }
                    });
                }
            }
            catch (error) {
                console.log(`${errors.tryCatch}cancellationRequest`, error);
                return res.json(responses.error);
            }
        }
    );
};